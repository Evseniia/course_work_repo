﻿
// DiplomDlg.cpp: файл реализации
//
#pragma once
#include "pch.h"
#include "framework.h"
#include "Diplom.h"
#include "DiplomDlg.h"
#include "afxdialogex.h"
#include "FUNC.h"
#include "SIGNAL_GENERATION.h"
#include "OTHER_FUNC.h"
#include "RESEARCH.h" 
#include <vector>
#include <ctime>
#define M_PI 3.1415926535

#define SINUSOIDA(x,ampl,chst,phasa) ampl*sin(2*M_PI*chst*x+phasa)
#define KOORD_Signal1(x,y) (xp1*((x)-xmin1)),(yp1*((y)-ymax1))   // для пересчета координат 

using namespace std;


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// Диалоговое окно CDiplomDlg

CDiplomDlg::CDiplomDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIPLOM_DIALOG, pParent)
	, bitrate(960)
	, chast_diskr(6e4)
	, chastota(3e3)
	, Amplituda(10)
	, delta_t(100)
	, kolvo(10)
	, delta_t_finded(0)
	, delta_w(5000)
	, noise(10)
	, delta_t_finded_1(0)
	, delta_w_finded(0)
	, FM2(FALSE)
	, FM4(FALSE)
	, status(FALSE)
	, KAM16(FALSE)
	, MSK(TRUE)
	, dispersion(FALSE)
	, delta_fff(0)
	, search_time()
	, choise_method(1)
	, error_fur(0)
	, porog(0.3)
	, offset(FALSE)
	, error_method(FALSE)
	, dec_iter(1)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDiplomDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, bitrate);
	DDX_Text(pDX, IDC_EDIT2, chast_diskr);
	DDX_Text(pDX, IDC_EDIT3, chastota);
	DDX_Text(pDX, IDC_EDIT4, Amplituda);
	DDX_Text(pDX, IDC_EDIT5, delta_t);
	DDX_Text(pDX, IDC_EDIT6, delta_t_finded);
	DDX_Text(pDX, IDC_EDIT7, delta_w);
	DDX_Text(pDX, IDC_EDIT8, noise);
	DDX_Text(pDX, IDC_EDIT9, delta_t_finded_1);
	DDX_Text(pDX, IDC_EDIT10, delta_w_finded);
	DDX_Check(pDX, IDC_CHECK2, FM2);
	DDX_Check(pDX, IDC_CHECK3, FM4);
	DDX_Check(pDX, IDC_CHECK1, status);
	DDX_Check(pDX, IDC_CHECK4, KAM16);
	DDX_Check(pDX, IDC_CHECK5, MSK);
	DDX_Check(pDX, IDC_CHECK6, dispersion);
	DDX_Text(pDX, IDC_EDIT11, delta_fff);
	DDX_Control(pDX, IDC_Signal1, Obj_Ris);
	DDX_Text(pDX, IDC_EDIT12, search_time);
	DDX_Text(pDX, IDC_EDIT13, choise_method);
	DDX_Text(pDX, IDC_EDIT14, kolvo);
	DDX_Text(pDX, IDC_EDIT15, error_fur);
	DDX_Text(pDX, IDC_EDIT16, porog);
	DDX_Check(pDX, IDC_CHECK7, offset);
	DDX_Check(pDX, IDC_CHECK8, error_method);
	DDX_Text(pDX, IDC_EDIT17, dec_iter);
}

BEGIN_MESSAGE_MAP(CDiplomDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CDiplomDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CDiplomDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CDiplomDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CDiplomDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CDiplomDlg::OnBnClickedButton5)
END_MESSAGE_MAP()


// Обработчики сообщений CDiplomDlg

BOOL CDiplomDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию
	UpdateData(false);

	xmin1 = 0;
	xmax1 = 1000;
	ymax1 = 100;
	ymin1 = -0.2 * ymax1;
	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CDiplomDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CDiplomDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CDiplomDlg::OnBnClickedButton1() // РАСЧЕТ
{
	UpdateData(TRUE);

	Signal_delta_t.clear();
	Signal_delta_w.clear();

	Signal_1.clear();
	Signal_2.clear();
	Music.clear();
	freq_shift_capon.clear();
	freq_shift_music.clear();
	freq_shift_fur.clear();
	error_capon.clear();
	error_music.clear();
	error_furie.clear();

	if (!dispersion && !offset && !error_method)
	{
		double start_time = clock(); // начальное время 
		Choice_modulation();

		addNoize_Complex(Signal_1, noise);
		addNoize_Complex(Signal_2, noise);

		shag_decimation = 1;
		Func_neopr_modified(Signal_delta_w, Signal_1, Signal_2, shag_decimation, chast_diskr, choise_method, porog, max_fur, max_w);
		vector<vector<double>> draw_vector;
		draw_vector.resize(1);
		draw_vector[0] = Signal_delta_w;
		//MyViewerDraw("Signal_delta_w", draw_vector, 0, Signal_delta_w.size(), Obj_Ris, "Signal_delta_w.png", false);
		//max_w = Func_neopr_Complex_Fur(Signal_delta_t, Signal_delta_w, Signal_1, Signal_2, shag_decimation, chast_diskr);

		double max_i_w = 0, max_i_interpolation = 0, error = 0;

		//kv_interpolation(Signal_delta_w, max_i_w, max_i_interpolation, Signal_delta_w.size());

		if (status) // временной сдвиг
		{
			delta_t_finded_1 = max_t;
			delta_t_finded = delta_t_finded_1 * period_diskr;
			vector<vector<double>> draw_vector;
			draw_vector.resize(1);
			draw_vector[0] = Signal_delta_t;
			//MyViewerDraw("Func neopr", draw_vector, 0, Signal_delta_t.size(), Obj_Ris, "MyViewerDraw.png", false);

		}

		else // частотный сдвиг
		{

			delta_w_finded = max_w;
			error = abs(delta_w_finded - delta_w);
			delta_fff = (chast_diskr / Signal_delta_w.size()) * (abs(max_i_interpolation) / shag_decimation);//abs(max_i_w - max_i_interpolation) ;
			error_fur = max_fur;

			vector <double> buf;
			buf.clear();
			for (int k = 0; k < Signal_delta_w.size(); k++)
			{
				buf.push_back((Signal_delta_w[(Signal_delta_w.size() - 1) - k]));
			}
			Signal_delta_w.clear();
			Signal_delta_w = buf;
			buf.clear();

			double end_time = clock(); // конечное время
			search_time = (end_time - start_time) / 1000.; // искомое время в сек


		}

	}

	if (dispersion || error_method) // ДИСПЕРСИЯ
	{
		double start_time = clock(); // начальное время 
		if (choise_method == 2) dispersion_capon(freq_shift_capon, noise, error_capon);
		if (choise_method == 1) dispersion_music(freq_shift_music, noise, porog, error_music);
		if (choise_method == 0) dispersion_fur(freq_shift_fur, noise, error_furie);

		MessageBoxA(NULL, "Calculations are finished", "Message", MB_OK);
		double end_time = clock(); // конечное время
		search_time = (end_time - start_time) / 1000.; // искомое время в сек

	}

	if (offset) // смещение оценки
	{
		double start_time = clock(); // начальное время 

		if (choise_method == 2) Freq_shift_est_offset_capon(freq_shift_capon, noise);
		if (choise_method == 1) Freq_shift_est_offset_music(freq_shift_music, noise, porog);
		if (choise_method == 0) Freq_shift_est_offset_fur(freq_shift_fur, noise);

		MessageBoxA(NULL, "Calculations are finished", "Message", MB_OK);
		double end_time = clock(); // конечное время
		search_time = (end_time - start_time) / 1000.; // искомое время в сек
	}

	//Risunok();

	UpdateData(FALSE);
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////
void CDiplomDlg::Risunok()
{

	Obj_Ris.clearAllRanges();
	DoubleArray Arr_dataReal;
	DoubleArray timeStamps;
	vector<double>Datatime;

	if (!status && !dispersion && !offset && !error_method) // отрисовка частотного сдвига
	{
		Arr_dataReal = vectorToArray(Signal_delta_w);
		double step = chast_diskr / Signal_delta_w.size();
		double www = Signal_delta_w.size() / chast_diskr;
		double shag_ox = (2 * DIAPOZON) / (Signal_delta_w.size() - 1);
		for (double i = max_fur - DIAPOZON; i <= max_fur + DIAPOZON; i += shag_ox) Datatime.push_back(i);//* step / shag_decimation);
		timeStamps = vectorToArray(Datatime);
		//for (double i = 0; i <= Signal_delta_w.size(); i += 1) Datatime.push_back(i/www);//* step / shag_decimation);
		//timeStamps = vectorToArray(Datatime);

		//vector<double> Signal_real; Signal_real.resize(Signal_2.size());
		//for (int i = 0; i < Signal_2.size(); i++) Signal_real[i] = Signal_2[i].real();
		//Arr_dataReal = vectorToArray(Signal_real);
		//for (int i = 0; i < Signal_real.size(); i++) Datatime.push_back(i);//* step / shag_decimation);
		//timeStamps = vectorToArray(Datatime);

	}
	if (status && !dispersion && !offset && !error_method) // отрисовка временного сдвига
	{
		Arr_dataReal = vectorToArray(Signal_delta_t);
		for (int i = 0; i < Signal_delta_t.size(); i++)Datatime.push_back(i);
		timeStamps = vectorToArray(Datatime);
	}

	if (offset) // отрисовка смещения
	{
		if (choise_method == 2) Arr_dataReal = vectorToArray(freq_shift_capon);
		if (choise_method == 1) Arr_dataReal = vectorToArray(freq_shift_music);
		if (choise_method == 0) Arr_dataReal = vectorToArray(freq_shift_fur);
		for (double i = 1; i < 250; i += i)
		{
			Datatime.push_back(i);
			if (i == 1) i = 16;

		}
		timeStamps = vectorToArray(Datatime);
	}
	if (dispersion) // отрисовка дисперсии
	{
		if (choise_method == 2) Arr_dataReal = vectorToArray(freq_shift_capon);
		if (choise_method == 0) Arr_dataReal = vectorToArray(freq_shift_fur);
		if (choise_method == 1) Arr_dataReal = vectorToArray(freq_shift_music);
		for (double i = 1; i < 250; i += i)
		{
			Datatime.push_back(i);
			if (i == 1) i = 16;

		}
		timeStamps = vectorToArray(Datatime);
	}
	if (error_method) // отрисовка ско
	{
		if (choise_method == 2) Arr_dataReal = vectorToArray(error_capon);
		if (choise_method == 0) Arr_dataReal = vectorToArray(error_furie);
		if (choise_method == 1) Arr_dataReal = vectorToArray(error_music);
		for (double i = 1; i < 250; i += i)
		{
			Datatime.push_back(i);
			if (i == 1) i = 16;

		}
		timeStamps = vectorToArray(Datatime);
	}

	XYChart* c = new XYChart(1100, 650);

	c->setPlotArea(70, 70, 1000, 500, Chart::Transparent, -1, Chart::Transparent, 0xcccccc);
	LegendBox* b = c->addLegend(70, 35, false, "arial.ttf", 12);
	b->setBackground(Chart::Transparent, Chart::Transparent);
	b->setLineStyleKey();

	c->xAxis()->setLabelStyle("arial.ttf", 12);
	c->yAxis()->setLabelStyle("arial.ttf", 12);

	c->xAxis()->setColors(Chart::Transparent, Chart::TextColor, Chart::TextColor, 0xaaaaaa);
	c->yAxis()->setColors(Chart::Transparent);

	c->xAxis()->setTickLength(10, 0);

	c->xAxis()->setTickDensity(20);
	c->yAxis()->setTickDensity(50);

	LineLayer* layer = c->addLineLayer();
	layer->setLineWidth(3);

	if (!dispersion && !offset && !error_method)
	{
		if (choise_method == 1) layer->addDataSet(Arr_dataReal, 0x5588cc, "Func_Neopr Music");
		if (choise_method == 2) layer->addDataSet(Arr_dataReal, 0x5588cc, "Func_Neopr Capon");
	}

	if (dispersion)
	{
		if (choise_method == 2)
		{
			layer->addDataSet(Arr_dataReal, 0x5588cc, "Error ");
			c->addTitle("Dispersion of estimation by capon method", "arial.ttf", 20, 0x555555);
			layer->setDataLabelFormat("{value|6} Hz^2");
		}
		if (choise_method == 0)
		{
			layer->addDataSet(Arr_dataReal, 0x5588cc, "Error ");
			c->addTitle("Dispersion of estimation by fur method", "arial.ttf", 20, 0x555555);
			layer->setDataLabelFormat("{value|6} Hz^2");
		}
		if (choise_method == 1)
		{
			layer->addDataSet(Arr_dataReal, 0x5588cc, "Error ");
			c->addTitle("Dispersion of estimation by music method", "arial.ttf", 20, 0x555555);
			layer->setDataLabelFormat("{value|6} Hz^2");
		}
	}

	if (error_method)
	{
		if (choise_method == 2)
		{
			layer->addDataSet(Arr_dataReal, 0x5588cc, "Error ");
			c->addTitle("Standard error by capon method", "arial.ttf", 20, 0x555555);
			layer->setDataLabelFormat("{value|6} Hz");
		}
		if (choise_method == 0)
		{
			layer->addDataSet(Arr_dataReal, 0x5588cc, "Error ");
			c->addTitle("Standard error by fur method", "arial.ttf", 20, 0x555555);
			layer->setDataLabelFormat("{value|6} Hz");
		}
		if (choise_method == 1)
		{
			layer->addDataSet(Arr_dataReal, 0x5588cc, "Error ");
			c->addTitle("Standard error by music method", "arial.ttf", 20, 0x555555);
			layer->setDataLabelFormat("{value|6} Hz");
		}
	}
	if (offset)
	{
		if (choise_method == 2)
		{
			layer->addDataSet(Arr_dataReal, 0x5588cc, "Error ");
			c->addTitle("Frequency shift estimation by capon method", "arial.ttf", 20, 0x555555);
			layer->setDataLabelFormat("{value|6} Hz");
		}

		if (choise_method == 1)
		{
			layer->addDataSet(Arr_dataReal, 0x5588cc, "Error ");
			c->addTitle("Frequency shift estimation by music method", "arial.ttf", 20, 0x555555);
			layer->setDataLabelFormat("{value|6} Hz");
		}
		if (choise_method == 0)
		{
			layer->addDataSet(Arr_dataReal, 0x5588cc, "Error ");
			c->addTitle("Frequency shift estimation by fur method", "arial.ttf", 20, 0x555555);
			layer->setDataLabelFormat("{value|6} Hz");
		}

	}
	layer->setXData(timeStamps);

	// Output the chart
	//c->makeChart("multiline2.png");

	Obj_Ris.setChart(c);
	delete c;
	Signal_delta_t.clear();
	Signal_delta_w.clear();
	freq_shift_capon.clear();
	freq_shift_music.clear();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CDiplomDlg::Choice_modulation()
{
	if (!FM2 && !FM4 && !KAM16 && !MSK)
	{
		MessageBox(L"Выберите вид молуляции", L"", MB_OK | MB_ICONINFORMATION);
	}

	if (MSK)
	{

		MSK_Signals_Generator(Signal_1, Signal_2, kolvo, delta_t, bitrate, chast_diskr, chastota);
		Dopler_shift(Signal_2, chast_diskr, delta_w);

	}

	if (FM2)
	{
		MessageBox(L"В разработке", L"", MB_OK | MB_ICONINFORMATION);
	}
	if (FM4)
	{
		MessageBox(L"В разработке", L"", MB_OK | MB_ICONINFORMATION);
	}
	if (KAM16)
	{
		MessageBox(L"В разработке", L"", MB_OK | MB_ICONINFORMATION);
	}


}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
DoubleArray CDiplomDlg::vectorToArray(std::vector<double>& v)
{
	return (v.size() == 0) ? DoubleArray() : DoubleArray(v.data(), (int)v.size());
}

void CDiplomDlg::MyViewerDraw(string Data, vector<vector<double>>& data, double Xmin, double Xmax, CChartViewer& viewer_num, string PathPic, bool podpisi)
{
	if (data.empty())return;
	// In this example, we simply use random data for the 3 data series.
	vector<DoubleArray> Arr_dataReal; Arr_dataReal.resize(data.size());
	for (int i = 0; i < data.size(); i++)
	{
		Arr_dataReal[i] = vectorToArray(data[i]);
	}

	vector<double>Datatime;

	double OXstep = (Xmax - Xmin) / (data[0].size() - 1);
	for (double i = Xmin; i <= Xmax; i += OXstep)Datatime.push_back(i);
	DoubleArray timeStamps = vectorToArray(Datatime);

	// Create a XYChart object of size 600 x 400 pixels
	XYChart* c = new XYChart(1100, 650);

	// Add a title box using grey (0x555555) 20pt Arial font
	//c->addTitle("", "arial.ttf", 20, 0x555555);

	// Set the plotarea at (70, 70) and of size 500 x 300 pixels, with transparent background and
	// border and light grey (0xcccccc) horizontal grid lines
	c->setPlotArea(70, 70, 1000, 500, Chart::Transparent, -1, Chart::Transparent, 0xcccccc);

	// Add a legend box with horizontal layout above the plot area at (70, 35). Use 12pt Arial font,
	// transparent background and border, and line style legend icon.
	LegendBox* b = c->addLegend(70, 35, false, "arial.ttf", 12);
	b->setBackground(Chart::Transparent, Chart::Transparent);
	b->setLineStyleKey();

	// Set axis label font to 12pt Arial
	c->xAxis()->setLabelStyle("arial.ttf", 12);
	c->yAxis()->setLabelStyle("arial.ttf", 12);

	// Set the x and y axis stems to transparent, and the x-axis tick color to grey (0xaaaaaa)
	c->xAxis()->setColors(Chart::TextColor, Chart::TextColor, Chart::TextColor, 0xaaaaaa);
	c->yAxis()->setColors(Chart::TextColor);

	// Set the major/minor tick lengths for the x-axis to 10 and 0.
	c->xAxis()->setTickLength(10, 0);

	// For the automatic axis labels, set the minimum spacing to 80/40 pixels for the x/y axis.
	c->xAxis()->setTickDensity(80);
	c->yAxis()->setTickDensity(40);

	// Add a title to the y axis using dark grey (0x555555) 14pt Arial font
	c->yAxis()->setTitle("", "arial.ttf", 14, 0x555555);

	// Add a line layer to the chart with 3-pixel line width
	LineLayer* layer = c->addLineLayer();
	layer->setLineWidth(3);
	//
	if (podpisi) layer->setDataLabelFormat("{value|0} Hz");
	// Add 3 data series to the line layer
	for (int i = 0; i < Arr_dataReal.size(); i++)
	{
		stringstream ss;
		ss << Data << i;
		string name = ss.str();
		layer->addDataSet(Arr_dataReal[i], -1, name.c_str());
	}
	// The x-coordinates for the line layer
	layer->setXData(timeStamps);
	viewer_num.setChart(c);
	const char* chPathPic = PathPic.c_str();
	c->makeChart(chPathPic);
	delete c;
}


void CDiplomDlg::OnBnClickedButton2()
{
	UpdateData(1);
	double min_freq = 0;
	double max_freq = 10000;
	double start_time = clock(); // конечное время
	MSK_Signals_Generator(Signal_1, Signal_2, kolvo, delta_t, bitrate, chast_diskr, chastota);
	Dopler_shift(Signal_2, chast_diskr, delta_w);
	addNoize_Complex(Signal_1, noise); // шум
	addNoize_Complex(Signal_2, noise);
	vector<double> pic;
	delta_w_finded = Filter_Bank_Viewpoint(Signal_1, Signal_2, min_freq, max_freq, 1., chast_diskr, pic);
	//Func_neopr_modified(pic, Signal_1, Signal_2, 1, chast_diskr, 1, porog, max_fur, delta_w_finded);
	double end_time = clock(); // конечное время
	search_time = (end_time - start_time) / 1000.; // искомое время в сек
	vector<vector<double>> draw_vector;
	draw_vector.resize(1);
	draw_vector[0] = pic;
	MyViewerDraw("Data", draw_vector, min_freq, max_freq, Obj_Ris, "MyViewerDraw.png", false);

	UpdateData(0);
}
void CDiplomDlg::OnBnClickedButton3()
{
	UpdateData(1);
	double min_freq = 3000;
	double max_freq = 7000;
	MSK_Signals_Generator(Signal_1, Signal_2, kolvo, delta_t, bitrate, chast_diskr, chastota);
	Dopler_shift(Signal_2, chast_diskr, delta_w);
	addNoize_Complex(Signal_1, noise); // шум
	addNoize_Complex(Signal_2, noise);
	vector<double> pic;
	delta_w_finded = Filter_Bank_Viewpoint_v2(Signal_1, Signal_2, min_freq, max_freq, 1, chast_diskr, pic, dec_iter);
	vector<vector<double>> draw_vector;
	draw_vector.resize(1);
	draw_vector[0] = pic;
	MyViewerDraw("Data", draw_vector, min_freq, max_freq, Obj_Ris, "MyViewerDraw.png", false);
	UpdateData(0);
}

void CDiplomDlg::OnBnClickedButton5() //button for convolutions test
{
	UpdateData(1);
	treug(Signal_1, 10);
	treug(Signal_2, 100);
	vecc_d result;
	//convolution_omp(Signal_1, Signal_2, result);
	sectional_convolution(Signal_1, Signal_2, result);
	vector<vector<double>> draw_vector;
	draw_vector.resize(1);
	draw_vector[0].resize(result.size());
	for (int i = 0; i < result.size(); i++) draw_vector[0][i] = abs(result[i]);
	MyViewerDraw("convolution_test", draw_vector, 0, result.size(), Obj_Ris, "convolution_test.png", false);
	UpdateData(0);
}

void CDiplomDlg::OnBnClickedButton4()
{
	UpdateData(1);
	double min_freq = 0;
	double max_freq = 10000;
	int noize_min = -10;
	int noize_max = 20;
	int noize_step = 5;
	vector<double> study_time;
	vector<double> study_fur;
	vector<double> pic;
	int try_size = 50;
	//for (double i = noize_min; i < noize_max; i += noize_step)
	//{
	//	double buffer = 0;
	//	for (int j = 0; j < try_size; j++)
	//	{
	//		MSK_Signals_Generator(Signal_1, Signal_2, kolvo, delta_t, bitrate, chast_diskr, chastota);
	//		double delta_w = 2000 + (rand() / RAND_MAX) * 6000;
	//		Dopler_shift(Signal_2, chast_diskr, delta_w);
	//		addNoize_Complex(Signal_1, i); // шум
	//		addNoize_Complex(Signal_2, i);
	//		delta_w_finded = Filter_Bank_Viewpoint(Signal_1, Signal_2, min_freq, max_freq, 1., chast_diskr, pic/*, i + 1*/);
	//		buffer += pow((delta_w_finded - delta_w), 2);
	//	}
	//	buffer /= try_size;
	//	study_time.push_back(sqrt(buffer));
	//}
	study_time = { 75.,63.,58,36,30 };
	for (double i = noize_min; i < noize_max; i += noize_step)
	{
		double buffer = 0;
		for (int j = 0; j < try_size; j++)
		{
			MSK_Signals_Generator(Signal_1, Signal_2, kolvo, delta_t, bitrate, chast_diskr, chastota);
			double delta_w_min = chast_diskr / (2. * step2(Signal_1.size()));
			double delta_w_max = chast_diskr / 2.;
			double delta_w = delta_w_min + ((double)rand() / RAND_MAX) * (delta_w_max - delta_w_min);// частотный сдвиг
			//double delta_w = 1000 + (rand() / RAND_MAX) * 6000;
			Dopler_shift(Signal_2, chast_diskr, delta_w);
			addNoize_Complex(Signal_1, i); // шум
			addNoize_Complex(Signal_2, i);
			delta_w_finded = Func_neopr_Complex_Fur(Signal_delta_t, Signal_delta_w, Signal_1, Signal_2, 1., chast_diskr);
			auto buffer_debug = delta_w_finded - delta_w;
			buffer += pow((buffer_debug), 2);;
		}
		buffer /= try_size;
		study_fur.push_back(sqrt(buffer));
	}
	//study_time.erase(study_time.begin(), study_time.begin() + 1);
	study_fur.erase(study_fur.begin(), study_fur.begin() + 1);
	vector<vector<double>> draw_vector;
	draw_vector.resize(2);
	draw_vector[0] = study_time;
	draw_vector[1] = study_fur;
	MyViewerDraw("studySNR", draw_vector, noize_min+ noize_step, noize_max, Obj_Ris, "studySNR.png", true);
	UpdateData(0);
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//UpdateData(1);
	//double min_freq = 0;
	//double max_freq = 10000;
	//int delta_t_min = 1000;
	//int delta_t_max = 10000;
	//int delta_t_step = 2000;
	//vector<double> study_time;
	//vector<double> study_fur;
	//vector<double> pic;
	//int try_size =5;
	//double old_result = 999999;
	//double result;
	//Signal_delta_t.clear();
	//Signal_delta_w.clear();
	//for (int i = delta_t_min; i < delta_t_max; i += delta_t_step)
	//{	
	//	double buffer = 0;
	//	for (int j = 0; j < try_size; j++)
	//	{
	//		MSK_Signals_Generator(Signal_1, Signal_2, i, 100, bitrate, chast_diskr, chastota);
	//		delta_w = 2000 + (rand() / RAND_MAX) * 6000;
	//		double delta_w_min = chast_diskr / (2. * step2(Signal_1.size()));
	//		double delta_w_max = chast_diskr / 2.;
	//		double delta_w = delta_w_min + ((double)rand() / RAND_MAX) * (delta_w_max - delta_w_min);// частотный сдвиг
	//		Dopler_shift(Signal_2, chast_diskr, delta_w);
	//		addNoize_Complex(Signal_1, noise); // шум
	//		addNoize_Complex(Signal_2, noise);
	//		delta_w_finded = Filter_Bank_Viewpoint(Signal_1, Signal_2, min_freq, max_freq, 1., chast_diskr, pic/*, i + 1*/);
	//		result = pow((delta_w_finded - delta_w), 2);

	//		if (result > old_result)
	//		{
	//			j--; continue;
	//		}else
	//		buffer += pow((delta_w_finded - delta_w), 2);
	//	}
	//	buffer /= try_size;
	//	old_result = buffer;
	//	study_time.push_back(sqrt(buffer));
	//}
	//for (int i = delta_t_min; i < delta_t_max; i += delta_t_step)
	//{
	//	double buffer = 0;
	//	for (int j = 0; j < try_size; j++)
	//	{
	//		MSK_Signals_Generator(Signal_1, Signal_2, i, 100, bitrate, chast_diskr, chastota);
	//		delta_w = 2000 + (rand() / RAND_MAX) * 6000;
	//		double delta_w_min = chast_diskr / (2. * step2(Signal_1.size()));
	//		double delta_w_max = chast_diskr / 2.;
	//		double delta_w = delta_w_min + ((double)rand() / RAND_MAX) * (delta_w_max - delta_w_min);// частотный сдвиг
	//		Dopler_shift(Signal_2, chast_diskr, delta_w);
	//		addNoize_Complex(Signal_1, noise); // шум
	//		addNoize_Complex(Signal_2, noise);
	//		delta_w_finded = Func_neopr_Complex_Fur(Signal_delta_t, Signal_delta_w, Signal_1, Signal_2, 1., chast_diskr);
	//		result = pow((delta_w_finded - delta_w), 2);
	//		buffer += pow((delta_w_finded - delta_w), 2);
	//	}
	//	buffer /= try_size;
	//	old_result = buffer;
	//	study_fur.push_back(sqrt(buffer));
	//}

	//vector<vector<double>> draw_vector;
	//draw_vector.resize(2);
	//draw_vector[0] = study_time;
	//draw_vector[1] = study_fur;
	//MyViewerDraw("study_Signal_size", draw_vector, delta_t_min, delta_t_max, Obj_Ris, "study_Signal_size.png", true);
	//UpdateData(0);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//UpdateData(1);
	//double min_freq = 0;
	//double max_freq = 10000;
	//int delta_t_min = 1000;
	//int delta_t_max = 10000;
	//int delta_t_step = 2000;
	//vector<double> study_time;
	//vector<double> study_fur;
	//vector<double> pic;
	//int try_size = 10;
	//double old_result = 999999;
	//double result;
	//Signal_delta_t.clear();
	//Signal_delta_w.clear();
	//for (int i = delta_t_min; i <= delta_t_max; i += delta_t_step)
	//{	
	//		MSK_Signals_Generator(Signal_1, Signal_2, 100, i, bitrate, chast_diskr, chastota);
	//		Dopler_shift(Signal_2, chast_diskr, delta_w);
	//		addNoize_Complex(Signal_1, noise); // шум
	//		addNoize_Complex(Signal_2, noise);
	//		double start_time = clock();
	//		delta_w_finded = Filter_Bank_Viewpoint(Signal_1, Signal_2, min_freq, max_freq, 1., chast_diskr, pic/*, i + 1*/);
	//		double end_time = clock(); // конечное время
	//		search_time = (end_time - start_time) / 1000.; // искомое время в сек
	//		study_time.push_back(search_time);
	//}
	//for (int i = delta_t_min; i <= delta_t_max; i += delta_t_step)
	//{
	//		MSK_Signals_Generator(Signal_1, Signal_2, 100, i, bitrate, chast_diskr, chastota);
	//		Dopler_shift(Signal_2, chast_diskr, delta_w);
	//		addNoize_Complex(Signal_1, noise); // шум
	//		addNoize_Complex(Signal_2, noise);
	//		double start_time = clock();
	//		delta_w_finded = Func_neopr_Complex_Fur(Signal_delta_t, Signal_delta_w, Signal_1, Signal_2, 1., chast_diskr);
	//		double end_time = clock(); // конечное время
	//		search_time = (end_time - start_time) / 1000.; // искомое время в сек
	//	    study_fur.push_back(search_time);
	//}

	//vector<vector<double>> draw_vector;
	//draw_vector.resize(2);
	//draw_vector[0] = study_time;
	//draw_vector[1] = study_fur;
	//MyViewerDraw("study_time", draw_vector, delta_t_min, delta_t_max, Obj_Ris, "study_time.png", true);
	//UpdateData(0);
}