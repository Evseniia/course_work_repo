﻿
// DiplomDlg.h: файл заголовка
//
#include <math.h>
#include "ChartViewer.h"
#include <complex>
#include <math.h>
#include <vector>
#include <fstream>
using namespace std;
#define M_PI 3.1415926535
#pragma once


// Диалоговое окно CDiplomDlg
class CDiplomDlg : public CDialogEx
{
// Создание
public:
	CDiplomDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIPLOM_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:

	double xp1, yp1,        // коэфициенты пересчета
		xmin1, xmax1,	    // максисимальное и минимальное значение первое окошко
		ymin1, ymax1;       // максисимальное и минимальное значение первое окошко
	CWnd* Child = new CWnd;
	// объекты класса CWnd
	CWnd* PicWndSignal1;

	// объекты класса CDC
	CDC* PicDcSignal1;

	// объекты класса CRect
	CRect PicSignal1;
	//ручки
	CPen osi_pen;
	CPen setka_pen;
	CPen graf_pen;
	CPen graf_pen1;
	CPen graf_penred;
	CPen graf_pengreen;
	CPen graf_penblue;


	vector<complex<double>> Signal_1; // сигнал
	vector<complex<double>> Signal_2; // сдвинутый сигнал
	vector<double> Signal_delta_t; // временной сдвиг
	vector<double> Signal_delta_w; // частотный сдвиг
	vector<double> R; // ММП
	vector <int> data; // биты данных
	//vector<double> B; // оценка частотного сдвига
	vector<double> freq_shift_capon; // вектор ошибок для метода кейпона
	vector<double> freq_shift_music; // вектор ошибок для метода music
	vector<double> freq_shift_fur; // вектор ошибок для метода fur
	vector<double> error_capon; // вектор ско для метода кейпона
	vector<double> error_music; // вектор ско для метода music
	vector<double> error_furie; // вектор ско для метода fur
	vector<double> Music; 
	vector<double> svertka; // частотный сдвиг


	double bitrate; // битрейт
	double chast_diskr; // частота дискретизации
	double chastota; // несущая частота
	double Amplituda; // амплитуда сигнала
	double delta_t; // временной сдвиг
	double delta_t_finded; // временной сдвиг ММП
	double delta_t_finded_1; // временной сдвиг ММП в секундах
	double delta_w_finded; // частотный сдвиг
	double delta_w; // частотный сдвиг
	double noise; // шум
	double period_diskr;
	double max_t, max_w;
	int shag_decimation;

	void Choice_modulation(); // выбор вида модуляции
	void Risunok(); // отрисовка

	BOOL status; // временной сдвиг
	BOOL FM2;
	BOOL FM4;
	BOOL KAM16;
	BOOL MSK;
	BOOL dispersion; // дисперсия оценки
	BOOL offset; // смещение оценки

	double box_1; // част. разр. при к=1
	double box_2; // част. разр. при к=2
	double box_3;// част. разр. при к=4

	double delta_fff; // ошибка опр. частн. сдвига.
	afx_msg void OnBnClickedButton1();
	CChartViewer Obj_Ris;
	DoubleArray vectorToArray(vector<double>& v);

	double search_time;
	int choise_method;
	double max_fur;
	
	int kolvo;// число отсчетов сигнала
	double error_fur;// сдвиг методом фурье
	double porog; // порог в процентах
	BOOL error_method; // ско методов
	/// <summary>
	/// ///////////2020
	/// </summary>
	afx_msg void OnBnClickedButton2();
	void MyViewerDraw(string Data, vector<vector<double>>& data, double Xmin, double Xmax, CChartViewer& viewer_num, string PathPic, bool podpis);
	afx_msg void OnBnClickedButton3();
	int dec_iter;
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton5();
	void treug(vector<complex<double>>& vec, int size)
	{
		vec.resize(size, 0.);
		for (int i = 0; i < size / 2; i++)
			vec[i] = i;
		for (int i = size / 2; i < vec.size(); i++)
			vec[i] = size - i;
	}
};
