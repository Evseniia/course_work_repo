#pragma once
#include "pch.h"
#include "framework.h"
#include "FUNC.h"
#include "SIGNAL_GENERATION.h"
#include "OTHER_FUNC.h"
#include "RESEARCH.h"
#include "afxdialogex.h"
#include <vector>

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void Freq_shift_est_offset_capon(vector<double>& freq_shift, double noise)
{
	double chast_diskr = 250000; // ������� �������������
	double max_j_fur, max_j_method; // ��������� ����� � ������ �������
	double porog = 0;
	int choise_method = 2; // ����� ������ �������
	int kolvo = 11000; // ����� ��������
	int delta_t = 100; // ��������� ��������
	int bitrate = 9600; // �������
	double chastota = 25000; // ������� �������
	//double delta_w = 5000;

	vector<complex<double>> Signal_1;
	vector<complex<double>> Signal_2;

	vector<double> Signal_delta_w;

	freq_shift.clear();

	vector<int> shag_k; // ������ ����� ����� ���������
	shag_k.clear();

	shag_k.push_back(1);
	for (int i = 32; i < 250; i += i)
	{
		shag_k.push_back(i);
	}

	for (int i = 0; i < shag_k.size(); i++)
	{	
		double ch_diskr_buf = chast_diskr / shag_k[i]; // ��������� ������� ������������� � � ���
		double delta_w_max = ch_diskr_buf / 2.;
		double delta_w_min = chast_diskr / 2 / shag_k[i] / 16384;
		srand(time(0));
		double delta_w = delta_w_min + ((double)rand() / RAND_MAX) * (delta_w_max - delta_w_min);// ��������� �����
		max_j_fur = 0;
		max_j_method = 0;
		Signal_delta_w.clear();
		Signal_1.clear();
		Signal_2.clear();

		MSK_Signals_Generator(Signal_1, Signal_2, kolvo, delta_t, bitrate, chast_diskr, chastota);
		Dopler_shift(Signal_2, chast_diskr, delta_w);

		addNoize_Complex(Signal_1, noise); // ���
		addNoize_Complex(Signal_2, noise);

		Func_neopr_modified(Signal_delta_w, Signal_1, Signal_2, shag_k[i], chast_diskr, \
			choise_method, porog, max_j_fur, max_j_method);

		double error = abs(max_j_method/*/ pow(10, 3) */- delta_w/*/ pow(10, 3)*/);
		//error /= pow(10, 3); // ������ � ���
		freq_shift.push_back(error);
	}

}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
void dispersion_capon(vector<double>& freq_shift, double noise, vector<double>& error)
{
	double chast_diskr = 250000; // ������� �������������
	double max_j_fur, max_j_method; // ��������� ����� � ������ �������
	double porog = 0;
	int choise_method = 2; // ����� ������ �������
	int kolvo = 11000; // ����� ��������
	int delta_t = 100; // ��������� ��������
	int bitrate = 9600; // �������
	double chastota = 25000; // ������� �������

	vector<complex<double>> Signal_1;
	vector<complex<double>> Signal_2;

	vector<double> Signal_delta_w;

	freq_shift.clear();
	error.clear();

	vector<int> shag_k; // ������ ����� ����� ���������
	shag_k.clear();

	shag_k.push_back(1);
	for (int i = 32; i < 250; i += i)
	{
		shag_k.push_back(i);
	}

	double buf;
	int kolvo_iter = 30;
	for (int i = 0; i < shag_k.size(); i++)
	{	
		buf = 0;
		double ch_diskr_buf = chast_diskr / shag_k[i]; // ��������� ������� ������������� � � ���
		double delta_w_max = ch_diskr_buf / 2.;
		double delta_w_min = chast_diskr/ (2 * shag_k[i] * 16384);
		for (int j = 0; j < kolvo_iter; j++)
		{
			srand(time(0));
			double delta_w = delta_w_min + (rand() / RAND_MAX) * (delta_w_max - delta_w_min);// ��������� �����
			max_j_fur = 0;
			max_j_method = 0;
			Signal_delta_w.clear();
			Signal_1.clear();
			Signal_2.clear();

			MSK_Signals_Generator(Signal_1, Signal_2, kolvo, delta_t, bitrate, chast_diskr, chastota);
			Dopler_shift(Signal_2, chast_diskr, delta_w);

			addNoize_Complex(Signal_1, noise); // ���
			addNoize_Complex(Signal_2, noise);

			Func_neopr_modified(Signal_delta_w, Signal_1, Signal_2, shag_k[i], chast_diskr, \
				choise_method, porog, max_j_fur, max_j_method);

			double error = pow(abs(max_j_method/*/ pow(10, 3)*/ - delta_w/*/ pow(10, 3)*/), 2);
			//error /= pow(10, 3); // ������ � k��
			buf += error;
		}
		buf /= kolvo_iter;
		freq_shift.push_back(buf);
		error.push_back(sqrt(buf));
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Freq_shift_est_offset_music(vector<double>& freq_shift, double noise, double porog)
{
	double chast_diskr = 250000; // ������� �������������
	double max_j_fur, max_j_method; // ��������� ����� � ������ �������
	int choise_method = 1; // ����� ������ music
	int kolvo = 11000; // ����� ��������
	int delta_t = 100; // ��������� ��������
	int bitrate = 9600; // �������
	double chastota = 25000; // ������� �������

	vector<complex<double>> Signal_1;
	vector<complex<double>> Signal_2;

	vector<double> Signal_delta_w;

	freq_shift.clear();

	vector<int> shag_k; // ������ ����� ����� ���������
	shag_k.clear();

	shag_k.push_back(1);
	for (int i = 32; i < 250; i += i)
	{
		shag_k.push_back(i);
	}

	for (int i = 0; i < shag_k.size(); i++)
	{
		double ch_diskr_buf = chast_diskr / shag_k[i]; // ��������� ������� ������������� � � ���
		double delta_w_max = ch_diskr_buf / 2.;
		double delta_w_min = chast_diskr / 2 / shag_k[i] / 16384;
		srand(time(0));
		double delta_w = delta_w_min + ((double)rand() / RAND_MAX) * (delta_w_max - delta_w_min);// ��������� �����
		max_j_fur = 0;
		max_j_method = 0;
		Signal_delta_w.clear();
		Signal_1.clear();
		Signal_2.clear();

		MSK_Signals_Generator(Signal_1, Signal_2, kolvo, delta_t, bitrate, chast_diskr, chastota);
		Dopler_shift(Signal_2, chast_diskr, delta_w);

		addNoize_Complex(Signal_1, noise); // ���
		addNoize_Complex(Signal_2, noise);

		Func_neopr_modified(Signal_delta_w, Signal_1, Signal_2, shag_k[i], chast_diskr, \
			choise_method, porog, max_j_fur, max_j_method);

		double error = abs(max_j_method/*/ pow(10, 3)*/ - delta_w/*/ pow(10, 3)*/);
		//error /= pow(10, 3); // ������ � ���
		freq_shift.push_back(error);
	}

}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void dispersion_music(vector<double>& freq_shift, double noise, double porog, vector<double>& error)
{
	double chast_diskr = 250000; // ������� �������������
	double max_j_fur, max_j_method; // ��������� ����� � ������ �������
	int choise_method = 1; // ����� ������ music
	int kolvo = 11000; // ����� ��������
	int delta_t = 100; // ��������� ��������
	int bitrate = 9600; // �������
	double chastota = 25000; // ������� �������

	vector<complex<double>> Signal_1;
	vector<complex<double>> Signal_2;

	vector<double> Signal_delta_w;

	freq_shift.clear();
	error.clear();

	vector<int> shag_k; // ������ ����� ����� ���������
	shag_k.clear();

	shag_k.push_back(1);
	for (int i = 32; i < 250; i += i)
	{
		shag_k.push_back(i);
	}

	double buf, buf_kv;
	int kolvo_iter = 15;
	for (int i = 0; i < shag_k.size(); i++)
	{
		buf = 0;
		buf_kv = 0;
		double ch_diskr_buf = chast_diskr / shag_k[i]; // ��������� ������� ������������� � � ���
		double delta_w_max = ch_diskr_buf / 2.;
		double delta_w_min = chast_diskr / (2 * shag_k[i] * 16384);
		for (int j = 0; j < kolvo_iter; j++)
		{
			//srand(time(0));
			double delta_w = delta_w_min + (rand() / RAND_MAX) * (delta_w_max - delta_w_min);// ��������� �����
			if (i == 0) delta_w = 5000;
			max_j_fur = 0;
			max_j_method = 0;
			Signal_delta_w.clear();
			Signal_1.clear();
			Signal_2.clear();

			MSK_Signals_Generator(Signal_1, Signal_2, kolvo, delta_t, bitrate, chast_diskr, chastota);
			Dopler_shift(Signal_2, chast_diskr, delta_w);

			addNoize_Complex(Signal_1, noise); // ���
			addNoize_Complex(Signal_2, noise);

			Func_neopr_modified(Signal_delta_w, Signal_1, Signal_2, shag_k[i], chast_diskr, \
				choise_method, porog, max_j_fur, max_j_method);

			double error = pow(abs(max_j_method /*/ pow(10, 3)*/ - delta_w /*/ pow(10, 3)*/), 2);
			double error_kv = sqrt(pow(abs(max_j_method - delta_w), 2));
			//error /= pow(10, 3); // ������ � k��
			buf += error;
			buf_kv += error_kv;
		}
		buf /= kolvo_iter;
		buf_kv /= kolvo_iter;
		freq_shift.push_back(buf);
		error.push_back(buf_kv);
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void Freq_shift_est_offset_fur(vector<double>& freq_shift, double noise)
{
	double chast_diskr = 250000; // ������� �������������
	double max_j_fur; // �������� ����� 
	int kolvo = 11000; // ����� ��������
	int delta_t = 100; // ��������� ��������
	int bitrate = 9600; // �������
	double chastota = 25000; // ������� �������

	vector<complex<double>> Signal_1;
	vector<complex<double>> Signal_2;

	vector<double> Signal_delta_w;
	vector<double> Signal_delta_t;

	freq_shift.clear();

	vector<int> shag_k; // ������ ����� ����� ���������
	shag_k.clear();

	shag_k.push_back(1);
	for (int i = 32; i < 250; i += i)
	{
		shag_k.push_back(i);
	}

	for (int i = 0; i < shag_k.size(); i++)
	{
		double ch_diskr_buf = chast_diskr / shag_k[i]; // ��������� ������� ������������� � � ���
		double delta_w_max = ch_diskr_buf / 2.;
		double delta_w_min = chast_diskr / 2 / shag_k[i] / 16384;
		srand(time(0));
		double delta_w = delta_w_min + ((double)rand() / RAND_MAX) * (delta_w_max - delta_w_min);// ��������� �����
		max_j_fur = 0;
		Signal_delta_w.clear();
		Signal_delta_t.clear();
		Signal_1.clear();
		Signal_2.clear();

		MSK_Signals_Generator(Signal_1, Signal_2, kolvo, delta_t, bitrate, chast_diskr, chastota);
		Dopler_shift(Signal_2, chast_diskr, delta_w);

		addNoize_Complex(Signal_1, noise); // ���
		addNoize_Complex(Signal_2, noise);

		max_j_fur = Func_neopr_Complex_Fur(Signal_delta_t, Signal_delta_w, Signal_1, Signal_2, \
			shag_k[i], chast_diskr);

		double max_i_w = 0, max_i_interpolation = 0;

		//kv_interpolation(Signal_delta_w, max_i_w, max_i_interpolation, Signal_delta_w.size());
		//max_i_w = (chast_diskr / Signal_delta_w.size()) * (abs(max_i_interpolation) / shag_k[i]);

		//double error = abs(max_j_fur - delta_w);
		double error  = abs(max_j_fur /*/ pow(10, 3) */- delta_w/*/ pow(10, 3)*/);
		//error /= pow(10, 3); // ������ � ���
		freq_shift.push_back(error);
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
void dispersion_fur(vector<double>& freq_shift, double noise, vector<double>& error)
{
	double chast_diskr = 250000; // ������� �������������
	double max_j_fur; // �������� ����� 
	int kolvo = 11000; // ����� ��������
	int delta_t = 100; // ��������� ��������
	int bitrate = 9600; // �������
	double chastota = 25000; // ������� �������

	vector<complex<double>> Signal_1;
	vector<complex<double>> Signal_2;

	vector<double> Signal_delta_w;
	vector<double> Signal_delta_t;

	freq_shift.clear();
	error.clear();

	vector<int> shag_k; // ������ ����� ����� ���������
	shag_k.clear();

	shag_k.push_back(1);
	for (int i = 32; i < 250; i += i)
	{
		shag_k.push_back(i);
	}

	double buf;
	int kolvo_iter = 25;
	for (int i = 0; i < shag_k.size(); i++)
	{
		buf = 0;
		double ch_diskr_buf = chast_diskr / shag_k[i]; // ��������� ������� ������������� � � ���
		double delta_w_max = ch_diskr_buf / 2.;
		double delta_w_min = chast_diskr / 2 / shag_k[i] / 16384;
		
		for (int j = 0; j < kolvo_iter; j++)
		{
			//srand(time(0));
			double delta_w = delta_w_min + ((double)rand() / RAND_MAX) * (delta_w_max - delta_w_min);// ��������� �����
			max_j_fur = 0;
			Signal_delta_w.clear();
			Signal_delta_t.clear();
			Signal_1.clear();
			Signal_2.clear();

			MSK_Signals_Generator(Signal_1, Signal_2, kolvo, delta_t, bitrate, chast_diskr, chastota);
			Dopler_shift(Signal_2, chast_diskr, delta_w);

			addNoize_Complex(Signal_1, noise); // ���
			addNoize_Complex(Signal_2, noise);

			max_j_fur = Func_neopr_Complex_Fur(Signal_delta_t, Signal_delta_w, Signal_1, Signal_2, \
				shag_k[i], chast_diskr);

			double max_i_w = 0, max_i_interpolation = 0;

			kv_interpolation(Signal_delta_w, max_i_w, max_i_interpolation, Signal_delta_w.size());
			max_i_w = (chast_diskr / Signal_delta_w.size()) * (abs(max_i_interpolation) / shag_k[i]);

			double error = pow(abs(max_i_w /*/ pow(10, 3)*/ - delta_w/*/ pow(10, 3)*/), 2);
			//error /= pow(10, 3); // ������ � k��
			buf += error;
		}
		buf /= kolvo_iter;
		freq_shift.push_back(buf);
		error.push_back(sqrt(buf));
	}
}