#pragma once
#include <vector>
#include <complex>
#include <math.h>
#include <fstream>
#include "afxdialogex.h"
#include <algorithm>
#include <ctime>
using namespace std;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
* �������� ������ ���������� ������ (����� �������)
* @param freq_shift - ������ ������
* @param noise - ���
*/
void Freq_shift_est_offset_capon(vector<double>& freq_shift, double noise);

/**
* ��������� ������ (����� �������)
* @param freq_shift - ������ ������
* @param noise - ���
* @param error - ������ ���
*/
void dispersion_capon(vector<double>& freq_shift, double noise, vector<double>& error);


///////////////////////////////////////////////////////////////////////////////////////////////////////
/**
* �������� ������ ���������� ������ (����� MUSIC)
* @param freq_shift - ������ ������
* @param noise - ���
* @param porog - ����� � ���������
*/
void Freq_shift_est_offset_music(vector<double>& freq_shift, double noise, double porog);

/**
* ��������� ������ (����� MUSIC)
* @param freq_shift - ������ ������
* @param noise - ���
* @param error - ������ ���
*/
void dispersion_music(vector<double>& freq_shift, double noise, double porog, vector<double>& error);


///////////////////////////////////////////////////////////////////////////////////////////////////////
/**
* �������� ������ ���������� ������ (����� �����)
* @param freq_shift - ������ ������
* @param noise - ���
*/
void Freq_shift_est_offset_fur(vector<double>& freq_shift, double noise);

/**
* ��������� ������ (����� �����)
* @param freq_shift - ������ ������
* @param noise - ���
* @param error - ������ ���
*/
void dispersion_fur(vector<double>& freq_shift, double noise, vector<double>& error);

