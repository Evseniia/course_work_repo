#pragma once
#include <vector>
#include <complex>
#include <math.h>
#include <fstream>
#include "afxdialogex.h"
#include <algorithm>
#include "csvd.h"
#define M_PI 3.1415926535
#define DIAPOZON (50.) 
#define DIAPOZON_DOTS 100
#define MUSIC 110
#define CAPON 90 //5, 90

using namespace std;
using vecc_d = vector<complex<double>>;
using vecc_f = vector<complex<float>>;

/**
* ������� ����������������
* @param mass_t - ������ ��� ��������� ��������
* @param mass_w - ������ ��� ��������� ��������
* @param Signal1 - ������ ������
* @param Signal2 - ������ ������
* @param k - ��� ���������
* @param chast_diskr - ������� �������������
*/
double Func_neopr_Complex_Fur(vector<double>& mass_t, vector<double>& mass_w,
	vector<complex<double>>& Signal1, vector<complex<double>>& Signal2,
	int k, double chast_diskr);


/**
* @param mass_w - ������ ��� ��������� ��������
* @param Signal1 - ������ ������
* @param Signal2 - ������ ������
* @param chast_diskr - ������� �������������
* @param max_fur - �������� �����
* @param porog - ����� � ���������
*/
double Func_neopr_Complex_Music(vector<double>& mass_w,
	vector<complex<double>>& Signal1, vector<complex<double>>& Signal2,
	double chast_diskr, double max_fur, double porog);


/**
* @param mass_w - ������ ��� ��������� ��������
* @param Signal1 - ������ ������
* @param Signal2 - ������ ������
* @param chast_diskr - ������� �������������
* @param max_fur - �������� �����
*/
double Func_neopr_Complex_Capon(vector<double>& mass_w,
	vector<complex<double>>& Signal1, vector<complex<double>>& Signal2,
	double chast_diskr, double max_fur);

////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
* ���������������� ������� ����������������
* @param mass_w - ������ ��� ��������� ��������
* @param Signal1 - ������ ������
* @param Signal2 - ������ ������
* @param k - ��� ���������
* @param chast_diskr - ������� �������������
* @param choise_method - (1) - Music
* @param porog - ����� � ���������
* @param max_j_fur - �������� �� ������� ������� �����
* @param max_j_method - �������� �� ������� �-� �������
*/
void Func_neopr_modified(vector<double>& mass_w, vector<complex<double>>& Signal1,
	vector<complex<double>>& Signal2, int k, double chast_diskr, int choise_method,
	double porog, double& max_j_fur, double& max_j_method);

////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
* ����� MUSIC
* @param Signal - ������ ������
* @param Music - ������
* @param chast_diskr - �������� �������������
* @param max_j - �������� �����
* @param shag_w - ��� �� �������
* @param porog - ����� � ���������
*/
void method_Music(const vector<complex<double>>& Signal,
	vector<complex<double>>& Music, double chast_diskr, double max_j, double shag_w, double porog);

////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
* ����� �������
* @param Signal - ������ ������
* @param Vec - ������
* @param chast_diskr - �������� �������������
* @param max_j - �������� �����
* @param shag_w - ��� �� �������
*/
void method_Capon(vector<complex<double>>& Signal,
	vector<complex<double >>& Vec, double chast_diskr, double max_j, double shag_w);

///////////////////////////////////////////////////
//////////////2020/////////////////////////////////
double Filter_Bank_Viewpoint(const vecc_d& Signal_1, const vecc_d& Signal_2,
	double freq_min, double freq_max,
	double freq_step, int sampling, vector<double>& pic);
double Filter_Bank_Viewpoint_v2(const vecc_d& Signal_1, const vecc_d& Signal_2,
	double &freq_min, double &freq_max,
	double freq_step, int sampling, vector<double>& pic,int iter);
void convolution_omp(vecc_d &h, vecc_d& x, vecc_d& result);
int circular_convolution_via_dft(vecc_d &h, vecc_d &x, vecc_d& result);
void sectional_convolution(vecc_d &h, vecc_d &x, vecc_d& result);
double Uncertainty_omp(vector<double>& mass_w,
	vector<complex<double>> Signal1, vector<complex<double>> Signal2,
	int ksum, double chast_diskr);
