#pragma once
#include "pch.h"
#include "framework.h"
#include "SIGNAL_GENERATION.h"
#include "afxdialogex.h"
#include <vector>
#define M_PI 3.1415926535

////////////////////////////////////////////////////////////////////////////////////////////////////////
void NormalPhaza(double& phaza)
{
	while (1)
	{
		if (phaza > 0)
		{
			if (phaza > (2 * M_PI))  phaza -= 2 * M_PI;
			else break;
		}
		else
		{
			if (phaza < (-2 * M_PI)) phaza += 2 * M_PI;
			else break;
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////
void MSK_Signals_Generator(vector <complex<double>>& Signal1, 
	vector <complex<double>>& Signal2, int signalSize, int delaySize, int bitrate, 
	int sampling, double local_frequencies)
{
	Signal1.clear();
	Signal2.clear();
	int bit_time = sampling / bitrate; //���-�� ������� �� 1 ���
	vector <int> SignalsObraz;
	SignalsObraz.resize(signalSize);
	// for ��� b_bit
	int interval = bit_time;//���-�� �������� �� 1 ���
	int buf_ii = 0;//�������
	double buf = 0 + 1000. * rand() / RAND_MAX;
	bool bit_buf;
	if (buf > 500) bit_buf = 1;
	else bit_buf = 0;
	for (int i = 0; i < SignalsObraz.size(); i++)
	{
		buf_ii++;
		SignalsObraz[i] = bit_buf;
		if (buf_ii == interval)
		{
			buf_ii = 0;
			buf = 0 + 1000. * rand() / RAND_MAX;
			bit_buf;
			if (buf > 500) bit_buf = 1;
			else bit_buf = 0;
		}
	}
	///���������� �������
	double Buffaza = 0;
	double delta4astota = bitrate / 4; // �������� �������
	Signal1.resize(signalSize);
	Signal2.resize(signalSize + delaySize);
	for (int i = 0; i < SignalsObraz.size(); i++)//����������� ������
	{//���������
		if (SignalsObraz[i])Buffaza += 2 * M_PI * (local_frequencies + delta4astota) / (double)sampling;
		else Buffaza += 2 * M_PI * (local_frequencies - delta4astota) / (double)sampling;
		NormalPhaza(Buffaza);
		Signal1[i] = (cos(Buffaza) + complex<double>(0, 1) * sin(Buffaza));
	}
	for (int i = delaySize; i < Signal2.size(); i++) //������� ������
		Signal2[i] = Signal1[i - delaySize];
}
////////////////////////////////////////////////////////////////////////////////////////////////////////
void Dopler_shift(vector<complex<double>>& mass, double sampling, double PhiDopler)
{
	for (int i = 0; i < mass.size(); i++)
	{
		mass[i] *= exp(complex<double>(0, 1) * 2. * M_PI * PhiDopler * (double)i / sampling);
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////
void Generation_signal_FM2_Complex(vector<complex<double>>& Data, double delay, int shag, int shag_ris, double delta_w, vector <int> data_bit, double Amplituda, double chastota, double chast_diskr)
{
	double buf = 0;
	int p = 0; // ��� ����
	int f = 0; // ��� data=0
	int iter = 0; // ���-�� ���
	int g = 0; // ��� data=1
	//Data.resize((shag_ris + delay) * data_bit.size());
	for (int k = 0; k < delay; k++)
	{
		buf += 0 * k;
		Data.push_back(buf);
	}

	int t = 0;
	buf = 0;
	for (int i = p; i < shag; i++)
	{

		if (data_bit[t] == 0)
		{

			if (f < shag_ris)
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				Data.push_back(Amplituda * (cos(buf) + complex<double>(0, 1) * sin(buf)));
				f++;

			}

		}

		if (data_bit[t] == 1)
		{
			if (g < shag_ris)
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				Data.push_back(-Amplituda * (cos(buf) + complex<double>(0, 1) * sin(buf)));
				g++;
			}
		}

		if (i == (shag - 1))
		{
			i = shag;
			shag += shag;
		}
		if (f >= shag_ris)
		{
			t++;
			f = 0;
			iter++;
		}
		if (g >= shag_ris)
		{
			t++;
			g = 0;
			iter++;
		}

		/*if (iter == data_bit.size())
		{
			break;
		}*/
		if (Data.size() == shag_ris * data_bit.size() + delay)
		{
			break;
		}


	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Generation_signal_FM4_Complex(vector<complex<double>>& Data, double delay, int shag, int shag_ris, double delta_w, vector <int>& data_bit, double Amplituda, double chastota, double chast_diskr)
{
	double buf = 0;
	double buf_2 = 0;
	double I = 0, Q = 0;
	int p = 0; // ��� ����
	int f = 0; // ��� data=00
	int iter = 0; // ���-�� ���
	int g = 0; // ��� data=01
	int gg = 0; // ��� data=11
	int ff = 0; // ��� data=10

	for (int k = 0; k < delay; k++)
	{
		//buf += 0 * k;
		Data.push_back(0);
	}

	int t = 0;
	for (int i = p; i < shag; i++)
	{

		if ((data_bit[t] == 0) && (data_bit[t + 1] == 0))
		{

			if (f < shag_ris)
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = cos(M_PI / 4.);
				Q = sin(M_PI / 4.);
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				f++;

			}

		}

		if ((data_bit[t] == 0) && (data_bit[t + 1] == 1))
		{
			if (g < shag_ris)
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = cos(-7. * M_PI / 4.);
				Q = sin(-7. * M_PI / 4.);
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				g++;
			}
		}
		if ((data_bit[t] == 1) && (data_bit[t + 1] == 1))
		{
			if (gg < shag_ris)
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = cos(-3. * M_PI / 4.);
				Q = sin(-3. * M_PI / 4.);
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				gg++;
			}
		}
		if ((data_bit[t] == 1) && (data_bit[t + 1] == 0))
		{
			if (ff < shag_ris)
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = cos(-M_PI / 4.);
				Q = sin(-M_PI / 4.);
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				ff++;
			}
		}

		if (i == (shag - 1))
		{
			i = shag;
			shag += shag;
		}
		if (f >= shag_ris)
		{
			t += 2;
			f = 0;
			iter++;
		}
		if (g >= shag_ris)
		{
			t += 2;
			g = 0;
			iter++;
		}
		if (ff >= shag_ris)
		{
			t += 2;
			ff = 0;
			iter++;
		}
		if (gg >= shag_ris)
		{
			t += 2;
			gg = 0;
			iter++;
		}

		if (iter == 100)
		{
			break;
		}


	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Generation_signal_MSK_Complex(vector<complex<double>>& Data, double delay, int shag, int shag_ris, double delta_w, vector <int>& data_bit, double Amplituda, double chastota, double chast_diskr)
{
	double buf = 0;
	int p = 0; // ��� ����
	int f = 0; // ��� data=0
	int iter = 0; // ���-�� ���
	int g = 0; // ��� data=1
	//Data.resize((shag_ris + delay) * data_bit.size());
	for (int k = 0; k < delay; k++)
	{
		buf += 0 * k;
		Data.push_back(buf);
	}

	int t = 0;
	for (int i = p; i < shag; i++)
	{
		double chastota_MSK = 15;

		if (data_bit[t] == 0)
		{

			if (f < shag_ris)
			{
				buf += 2. * M_PI * (chastota + chastota_MSK + delta_w) / chast_diskr;
				std::complex <double> buf_c = Amplituda * (cos(buf) + complex<double>(0, 1) * sin(buf));
				Data.push_back(buf_c);
				f++;

			}

		}

		if (data_bit[t] == 1)
		{
			if (g < shag_ris)
			{
				buf += 2. * M_PI * (chastota - chastota_MSK + delta_w) / chast_diskr;
				std::complex <double> buf_c = Amplituda * (cos(buf) + complex<double>(0, 1) * sin(buf));
				Data.push_back(buf_c);
				g++;
			}
		}

		if (i == (shag - 1))
		{
			i = shag;
			shag += shag;
		}
		if (f >= shag_ris)
		{
			t++;
			f = 0;
			iter++;
		}
		if (g >= shag_ris)
		{
			t++;
			g = 0;
			iter++;
		}

		if (iter == data_bit.size())
		{
			break;
		}


	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Generation_signal_KAM16_Complex(vector<complex<double>>& Data, double delay, int shag, int shag_ris, double delta_w, vector <int>& data_bit, double Amplituda, double chastota, double chast_diskr)
{
	double buf = 0;
	double buf_2 = 0;
	double I = 0, Q = 0;
	int p = 0; // ��� ����
	int f = 0;
	int iter = 0; // ���-�� ���
	int g = 0;

	for (int k = 0; k < delay; k++)
	{
		//buf += 0 * k;
		Data.push_back(0);
	}

	int t = 0;
	for (int i = p; i < shag; i++)
	{

		if ((data_bit[t] == 0) && (data_bit[t + 1] == 0) && (data_bit[t + 2] == 0) && (data_bit[t + 3] == 0))
		{

			if (f < shag_ris) //0000
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = -3;
				Q = 3;
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				f++;

			}

		}
		if ((data_bit[t] == 0) && (data_bit[t + 1] == 1) && (data_bit[t + 2] == 0) && (data_bit[t + 3] == 0))
		{
			if (g < shag_ris) //0100
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = -1;
				Q = 3;
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				g++;
			}
		}
		if ((data_bit[t] == 0) && (data_bit[t + 1] == 0) && (data_bit[t + 2] == 0) && (data_bit[t + 3] == 1))
		{
			if (f < shag_ris) //0001
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = -3;
				Q = 1;
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				f++;
			}
		}
		if ((data_bit[t] == 0) && (data_bit[t + 1] == 1) && (data_bit[t + 2] == 0) && (data_bit[t + 3] == 1))
		{
			if (g < shag_ris) //0101
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = -1;
				Q = 1;
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				g++;
			}
		}
		if ((data_bit[t] == 1) && (data_bit[t + 1] == 1) && (data_bit[t + 2] == 0) && (data_bit[t + 3] == 0))
		{
			if (f < shag_ris) //1100
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = 1;
				Q = 3;
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				f++;
			}
		}
		if ((data_bit[t] == 1) && (data_bit[t + 1] == 0) && (data_bit[t + 2] == 0) && (data_bit[t + 3] == 0))
		{
			if (g < shag_ris) //1000
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = 3;
				Q = 3;
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				g++;
			}
		}
		if ((data_bit[t] == 1) && (data_bit[t + 1] == 1) && (data_bit[t + 2] == 0) && (data_bit[t + 3] == 1))
		{
			if (f < shag_ris) //1101
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = 1;
				Q = 1;
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				f++;
			}
		}
		if ((data_bit[t] == 1) && (data_bit[t + 1] == 0) && (data_bit[t + 2] == 0) && (data_bit[t + 3] == 1))
		{
			if (g < shag_ris) //1001
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = 3;
				Q = 1;
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				g++;
			}
		}
		if ((data_bit[t] == 0) && (data_bit[t + 1] == 0) && (data_bit[t + 2] == 1) && (data_bit[t + 3] == 1))
		{
			if (f < shag_ris) //0011
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = -3;
				Q = -1;
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				f++;
			}
		}
		if ((data_bit[t] == 0) && (data_bit[t + 1] == 1) && (data_bit[t + 2] == 1) && (data_bit[t + 3] == 1))
		{
			if (g < shag_ris) //0111
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = -1;
				Q = -1;
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				g++;
			}
		}
		if ((data_bit[t] == 0) && (data_bit[t + 1] == 0) && (data_bit[t + 2] == 1) && (data_bit[t + 3] == 0))
		{
			if (f < shag_ris) //0010
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = -3;
				Q = -3;
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				f++;
			}
		}
		if ((data_bit[t] == 0) && (data_bit[t + 1] == 1) && (data_bit[t + 2] == 1) && (data_bit[t + 3] == 0))
		{
			if (g < shag_ris) //0110
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = -1;
				Q = -3;
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				g++;
			}
		}
		if ((data_bit[t] == 1) && (data_bit[t + 1] == 1) && (data_bit[t + 2] == 1) && (data_bit[t + 3] == 1))
		{
			if (f < shag_ris) //1111
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = 1;
				Q = -1;
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				f++;
			}
		}
		if ((data_bit[t] == 1) && (data_bit[t + 1] == 0) && (data_bit[t + 2] == 1) && (data_bit[t + 3] == 1))
		{
			if (g < shag_ris) //1011
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = 3;
				Q = -1;
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				g++;
			}
		}
		if ((data_bit[t] == 1) && (data_bit[t + 1] == 1) && (data_bit[t + 2] == 1) && (data_bit[t + 3] == 0))
		{
			if (f < shag_ris) //1110
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = 1;
				Q = -3;
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				f++;
			}
		}
		if ((data_bit[t] == 1) && (data_bit[t + 1] == 0) && (data_bit[t + 2] == 1) && (data_bit[t + 3] == 0))
		{
			if (g < shag_ris) //1010
			{
				buf = 2. * M_PI * delta_w * i / chast_diskr;
				I = 3;
				Q = -3;
				Data.push_back(Amplituda * ((I + complex<double>(0, 1) * Q) * (cos(buf) + complex<double>(0, 1) * sin(buf))));
				g++;
			}
		}

		if (i == (shag - 1))
		{
			i = shag;
			shag += shag;
		}
		if (f >= shag_ris)
		{
			t += 4;
			f = 0;
			iter++;
		}
		if (g >= shag_ris)
		{
			t += 4;
			g = 0;
			iter++;
		}


		if (iter == 100)
		{
			break;
		}


	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////