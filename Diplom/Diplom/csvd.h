/**************************************************************************************************************
 * File: csvd.h                                                                                               *
 * Author: Alexander Kreuz                                                                                    *
 * Last changed: 18.10.2019                                                                                   *
 **************************************************************************************************************/
#pragma once

#ifndef CSVD_H
#define CSVD_H

#include <vector>
#include <complex>
#include "csvd.h"
#include "spl.h"

namespace spl {

    class ComplexSingularValueDecompositionAlgorithm {
    public:
        //! \brief Pure virtual prototype for method calculating the CSVD for arbitrary complex matrices.
        //! \details All matrices and results are stored in single row array format.
        //! \param[in] a The matrix for which the SVD will be calculated.
        //! \param[in] m The amount of rows of the input matrix \p a.
        //! \param[in] n The amount of columns of the input matrix \p a.
        //! \param[out] u The left eigenvectors as a \p m x \p m matrix.
        //! \param[out] sigma The eigenvalues as \p m x \p n matrix.
        //! \param[out] v The right eigenvectors as \p n x \p n matrix.
        virtual void csvd(const std::vector<std::complex<double>>& a, unsigned int m, unsigned int n, std::vector<std::complex<double>>& u, std::vector<std::complex<double>>& sigma, std::vector<std::complex<double>>& v) const = 0;
        //! \brief Allows setting the precision. The default precision is 1.0E-6.
        //! \param[in] epsilon The new precision.
        void set_precision(const double epsilon);
        //! \brief Allows to define a new iteration limit. 
        //! \details The iteration limit can be used when implementing the CSVD to throw an exception when this limit is exceeded.
        //! The default iteration limit is set to 10.000 (ten thousand) iterations.
        void set_iteration_limit(const unsigned long long iteration_limit);
    protected:
        //! \brief Calculates the scalar product (or dot product) of two vectors \p a and \p b.
        //! \details Since this is the complex scalar product, elements of \p b are conjugated before multiplying.
        //! \exception std::invalid_argument if vectors \p a and \p b are of unequal dimensions.
        //! \param[in] a The left hand vector for which the scalar product shall be calculated.
        //! \param[in] b The right hand vector for which the scalar product shall be calculated.
        //! \return The complex scalar product of vectors \p a and \p b.
        std::complex<double> scalar_product(const std::vector<std::complex<double>>& a, const std::vector<std::complex<double>>& b) const noexcept(false);
        //! \brief Calculates the vector product (or cross product) of two vectors \p a and \p b.
        //! \exception std::invalid_argument if vectors \p a and \p b are of unequal dimensions.
        //! \param[in] a The left hand vector for which the vector product shall be calculated.
        //! \param[in] b The right hand vector for which the vector product shall be calculated.
        //! \return The complex vector product of vectors \p a and \p b.
        std::vector<std::complex<double>> vector_product(const std::vector<std::complex<double>>& a, const std::vector<std::complex<double>>& b) const noexcept(false);
        //! \brief Calculates the Euclidean norm of a given complex vector \p a.
        //! \param[in] a The vector for which the Euclidean norm shall be calculated.
        //! \return The complex vector norm of vector \p a.
        std::complex<double> vector_norm(const std::vector<std::complex<double>>& a) const;
        //! \brief Normalizes a given complex vector \p a using its Euclidean norm.
        //! \param[in] a The vector which shall be normalized.
        //! \return The normalized vector \p a. After normalization its norm will be 1.
        std::vector<std::complex<double>> vector_normalize(const std::vector<std::complex<double>>& a) const;
        //! \brief Builds an identity matrix of size \p n x \p n.
        //! \exception std::invalid_argument in case \p n is \p 0.
        //! \param[in] n The amount of rows and columns in the built identity matrix.
        //! \return An identity matrix with dimensions \p n x \p n.
        std::vector<std::complex<double>> matrix_build_identity(unsigned int n) const noexcept(false);
        //! \brief Transposes a complex matrix \p a without conjugating its entries.
        //! \details If you need the complex transposition of \p a, use \p matrix_conjugate_transpose instead.
        //! \warning If the given dimensions of the matrix are incorrect, the behaviour is undefined.
        //! \param[in] a The matrix that shall be transposed. 
        //! \param[in] n The dimensions of \p a (amount of rows and columns).
        //! \return The transposed matrix \p a.
        //! \sa ComplexSingularValueDecompositionAlgorithm#matrix_conjugate_transpose
        std::vector<std::complex<double>> matrix_transpose(const std::vector<std::complex<double>>& a, unsigned int n) const;
        //! \brief Transposes a complex matrix \p a and conjugates its entries.
        //! \details If you need the transposition of \p a without conjugation, use \p matrix_transpose instead.
        //! \warning If the given dimensions of the matrix are incorrect, the behaviour is undefined.
        //! \param[in] a The matrix that shall be transposed. 
        //! \param[in] n The dimensions of \p a (amount of rows and columns).
        //! \return The transposed matrix \p a.
        //! \sa ComplexSingularValueDecompositionAlgorithm#matrix_transpose
        std::vector<std::complex<double>> matrix_conjugate_transpose(const std::vector<std::complex<double>>& a, unsigned int n) const;
        //! \brief Multiplies two complex matrices \p a and \p b.
        //! \details Multiplication is only possible, if the amount of rows of \p a is equal to the amount of columns of \p b.
        //! \warning If the given dimensions of the matrices are incorrect, the behaviour is undefined.
        //! \param[in] a The left hand matrix of the multiplication.
        //! \param[in] b The right hand matrix of the multiplication.
        //! \param[in] ma The amount of rows of \p a.
        //! \param[in] na_mb The amount of columns of \p a and also the amount of rows of \p b.
        //! \param[in] nb The amount of columns of \p b.
        //! \return The matrix product of \p a and \p b.
        std::vector<std::complex<double>> matrix_multiply(const std::vector<std::complex<double>>& a, const std::vector<std::complex<double>>& b, unsigned int ma, unsigned int na_mb, unsigned int nb) const;
        //! \brief Creates a Householder transform matrix. 
        //! \details Multiplying this matrix with the vector \p x or a matrix containing it will make these elements equal to zero except for the first \p k elements.
        //! \warning If the given dimensions of the vector are incorrect, the behaviour is undefined.
        //! \param[in] x The vector of which the last elements starting from \p k shall be zeroed using the Householder transform.
        //! \param[in] n The length of \p x and also the dimensions of the matrix that will be multiplied by Householder's matrix.
        //! \param[in] k The starting index from which (inclusively) the elements of \p x shall be zeroed.
        //! \return The Householder transform matrix to make the elements of \p x equal to zero if multiplied by the matrix. All other elements remain unchanged.
        std::vector<std::complex<double>> matrix_householder_transform(const std::vector<std::complex<double>>& x, unsigned int n, unsigned int k) const;
        //! \brief Calculates the upper diagonal sum of a given matrix \p a. This is used to reach the needed precision when diagonalizing a bidiagonal matrix.
        //! \warning If the given dimensions of the matrix are incorrect, the behaviour is undefined.
        //! \param[in] a A bidiagonal matrix the upper diagonal sum of which shall be calculated.
        //! \param[in] m The amount of rows of \p a.
        //! \param[in] n The amount of columns of \p a.
        //! \return The sum of squares of the matrix entries on the upper diagonal of \p a.
        //! \sa ComplexSingularValueDecompositionAlgorithm#matrix_diagonalize
        //! \sa ComplexSingularValueDecompositionAlgorithm#matrix_bidiagonalize
        double matrix_upper_diagonal_sum(const std::vector<std::complex<double>>& a, unsigned int m, unsigned int n) const;
        //! \brief Diagonalizes a bidiagonal matrix \p a using Givens rotations.
        //! \details This method calculates the upper diagonal sum and compares it to the precision set by \p set_precision.
        //! You can use the method to set your own custom precision.
        //! \warning If the given dimensions of the matrix are incorrect or \p a is not a bidiagonal matrix, the behaviour is undefined.
        //! \exception std::runtime_error If the iteration limit set by \p set_iteration_limit is exceeded before reaching the required precision.
        //! Set a higher iteration limit or lower the precision in case this happens and try again.
        //! \param[in] a A bidiagonal matrix that shall be diagonalized.
        //! \param[in] m The amount of rows of \p a.
        //! \param[in] n The amount of columns of \p a.
        //! \param[out] u The left hand operator matrix saving all left hand Givens rotations applied to \p a during diagonalization.
        //! \param[out] b The diagonalized form of matrix \p a.
        //! \param[out] v The right hand operator matrix saving all right hand Givens rotation applied to \p a during diagonalization.
        //! \sa ComplexSingularValueDecompositionAlgorithm#set_precision
        //! \sa ComplexSingularValueDecompositionAlgorithm#set_iteration_limit
        //! \sa ComplexSingularValueDecompositionAlgorithm#givens_rotation
        //! \sa ComplexSingularValueDecompositionAlgorithm#matrix_bidiagonalize
        void matrix_diagonalize(const std::vector<std::complex<double>>& a, unsigned int m, unsigned int n, std::vector<std::complex<double>>& u, std::vector<std::complex<double>>& b, std::vector<std::complex<double>>& v) const noexcept(false);
        //! \brief Bidiagonalizes an arbitrary complex matrix \p a using Householder transformations.
        //! \details This method is not affected by the precision and iteration count limitations.
        //! \warning If the given dimensions of the matrix are incorrect, the behaviour is undefined.
        //! \param[in] a The complex matrix which shall be bidiagonalized.
        //! \param[in] m The amount of rows of \p a.
        //! \param[in] n The amount of columns of \p a.
        //! \param[out] u The left hand operator matrix saving all left hand Householder transformations applied to \p a during bidiagonalization.
        //! \param[out] b The diagonalized form of matrix \p a.
        //! \param[out] v The right hand operator matrix saving all right hand Householder transformations applied to \p a during bidiagonalization.
        //! \sa ComplexSingularValueDecompositionAlgorithm#matrix_householder_transform
        //! \sa ComplexSingularValueDecompositionAlgorithm#matrix_bidiagonalize
        void matrix_bidiagonalize(const std::vector<std::complex<double>>& a, unsigned int m, unsigned int n, std::vector<std::complex<double>>& u, std::vector<std::complex<double>>& b, std::vector<std::complex<double>>& v) const;
        //! \brief Calculates the sine and cosine values for rotating a complex vector components of which are given by \p f and \p g.
        //! \details Givens rotation applied to a vector consisting of two elements \p f and \p g will change \p f and set \p g to zero.
        //! \param[in] f The first element of the complex vector that shall be rotated.
        //! \param[in] g The second element of the complex vector that shall be rotated.
        //! \param[out] cos The cosine value of the angle by which the vector must be rotated.
        //! \param[out] sin The sine value of the angle by which the vector must be rotated.
        void givens_rotation(const std::complex<double>& f, const std::complex<double>& g, std::complex<double>& cos, std::complex<double>& sin) const;
    private:
        //! \brief Diagonalization precision.
        double _precision = 1.0E-6;
        //! \brief Iteration count limit for diagonalizing matrices.
        unsigned long long _iteration_limit = 10000ULL;
    };

    class FullCSVD : public ComplexSingularValueDecompositionAlgorithm {
    public:
        //! \brief Calculates the Singular Value Decomposition of an arbitrary complex matrix.
        //! \details All matrices and results are stored in single row array format.
        //! This method performs the so called Full SVD by sequentially applying Householder transformations and Givens rotations.
        //! \warning If the given dimensions of the matrix are incorrect, the behaviour is undefined.
        //! \exception std::runtime_error In case the iteration limit is exceeded.
        //! \param[in] a The matrix for which the SVD will be calculated.
        //! \param[in] m The amount of rows of the input matrix a.
        //! \param[in] n The amount of columns of the input matrix a.
        //! \param[out] u The left eigenvectors as a m x m matrix.
        //! \param[out] sigma The eigenvalues as m x n matrix.
        //! \param[out] v The right eigenvectors as n x n matrix.
        virtual void csvd(const std::vector<std::complex<double>>& a, unsigned int m, unsigned int n, std::vector<std::complex<double>>& u, std::vector<std::complex<double>>& sigma, std::vector<std::complex<double>>& v) const override final;
    };

    class ParallelCSVD : public ComplexSingularValueDecompositionAlgorithm {
    public:
        //! \brief Calculates the Singular Value Decomposition of an arbitrary complex matrix.
        //! \details All matrices and results are stored in single row array format.
        //! This method performs the so called Full SVD but is optimized for multicore processors using the Intel Threading Building Blocks library.
        //! \warning If the given dimensions of the matrix are incorrect, the behaviour is undefined.
        //! \exception std::runtime_error In case the iteration limit is exceeded.
        //! \param[in] a The matrix for which the SVD will be calculated.
        //! \param[in] m The amount of rows of the input matrix a.
        //! \param[in] n The amount of columns of the input matrix a.
        //! \param[out] u The left eigenvectors as a m x m matrix.
        //! \param[out] sigma The eigenvalues as m x n matrix.
        //! \param[out] v The right eigenvectors as n x n matrix.
        //! \sa FullCSVD#csvd
        virtual void csvd(const std::vector<std::complex<double>>& a, unsigned int m, unsigned int n, std::vector<std::complex<double>>& u, std::vector<std::complex<double>>& sigma, std::vector<std::complex<double>>& v) const override final {
            throw new std::runtime_error("Not yet implemented.");
        }
    };

}

#endif