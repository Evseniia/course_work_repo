#pragma once
#include <vector>
#include <complex>
#include <math.h>
#include <fstream>
#include "afxdialogex.h"
#include <algorithm>
#define M_PI 3.1415926535


using namespace std;

/**
* ���������� ����
* @param mass - ������ ������
* @param Noize - ������� ����
*/
void addNoize_Complex(vector<complex<double>>& mass, double Noize);

////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
* �������������� �����
* @param data - ������ ������
* @param is - (-1) - ������, (1) - ��������
*/
void fur(vector <complex <double>>& data, int is);

////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
*��������� ������� ������
*/
int step2(int sizein);
int step2(int sizein, int &i);

////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
* ������������ ������������
* @param mass - ������ ������
* @param max_i_w - �������� �� ������� ������
* @param max_i_interpolation - �������� ����� ������������
* @param size - ������ ������� ������
*/
void kv_interpolation(vector<double>& mass, double& max_i_w,
	double& max_i_interpolation, int size);

////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
* ����������� ����������
*/
int svd_hestenes(int m_m, int n_n, vector<double>& a,
	vector<double>& u, vector<double>& v, vector<double>& sigma);

int ___CSVD(vector<complex<double>> A, int M, int N, int NU, int NV, vector<double>& S,
	vector<complex<double>>& U, vector<complex<double>>& V);

////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
* ���������������� � ������������ ������
*/
void trans_matr(const vector<vector<complex<double>>>& v1, 
	const vector<vector<complex<double>>>& v2, vector<vector<complex<double>>>& v3);
void transpose_conj(vector<vector<complex<double>>>& v1);

////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
* ����� ������-������� ��� ������ �������� �������
* @param v1 - ������ ������
* @param razmer_matr_corr - ������ ������� ������
*/
void gauss_jordan(vector<vector<complex<double>>>& v1, int razmer_matr_corr);

////////////////////////////////////////////////////////////////////////////////////////////////////////