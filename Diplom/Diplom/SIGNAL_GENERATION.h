#pragma once
#include <vector>
#include <complex>
#include <math.h>
#include <fstream>
#include "afxdialogex.h"
#include <algorithm>
#define M_PI 3.1415926535

using namespace std;

/**
* ��������� ��������
* @param Data - ������
* @param delay - ��������� ��������
* @param shag - ���������� �������� � ����� ���� ������
* @param shag_ris - ���������� �������� � ����� ���� ������
* @param delta_w - �������� �� �������
* @param data_bit - ������ ������� ������
* @param Amplituda - ��������� �������
* @param chastota - ������� �������
* @param chast_diskr - �������� �������������
*/
void Generation_signal_FM2_Complex(vector<complex<double>>& Data, double delay, int shag,
	int shag_ris, double delta_w, vector <int> data_bit, double Amplituda, double chastota,
	double chast_diskr);
void Generation_signal_FM4_Complex(vector<complex<double>>& Data, double delay, int shag,
	int shag_ris, double delta_w, vector <int>& data_bit, double Amplituda, double chastota,
	double chast_diskr);
void Generation_signal_KAM16_Complex(vector<complex<double>>& Data, double delay, int shag,
	int shag_ris, double delta_w, vector <int>& data_bit, double Amplituda, double chastota,
	double chast_diskr);
//void Generation_signal_MSK_Complex(vector<complex<double>>& Data, double delay, int shag,
//	int shag_ris, double delta_w, vector <int>& data_bit, double Amplituda, double chastota,
//	double chast_diskr);

/**
* @param Signal1 - ������
* @param Signal2 - ������
* @param signalSize - ����� �������� ������� (������)
* @param delaySize - ��������� ��������
* @param bitrate - �������
* @param sampling - ������� �������������
* @param local_frequencies - ������� �������
*/
void MSK_Signals_Generator(vector <complex<double>>& Signal1, vector <complex<double>>& Signal2,
	int signalSize, int delaySize, int bitrate, int sampling, double local_frequencies);
////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
* ����������� ����
* @param phaza - ����
*/
void NormalPhaza(double& phaza);
////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
* ����� �� �������
* @param mass - ������
* @param sampling - ������� �������������
* @param PhiDopler - ����� �� �������
*/
void Dopler_shift(vector<complex<double>>& mass, double sampling, double PhiDopler);
////////////////////////////////////////////////////////////////////////////////////////////////////////

