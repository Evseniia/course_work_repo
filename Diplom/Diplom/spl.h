//! \file spl.h
//! \brief Main header file for the Signal Processing Library (SPL).
//! \author M. Alexander Kreuz
//! \date 2019 - 2020
#pragma once
#pragma warning (disable: 4251)

//! \def SPL_H
//! \brief Include guard for spl.h.
#ifndef SPL_H
#define SPL_H

//! \def SPL_API
//! DLL export/import linker directive.
#ifdef SPL_EXPORTS
#define SPL_API __declspec(dllexport)
#else
#define SPL_API __declspec(dllimport)
#endif

#include <vector>
#include <complex>

namespace spl {

    //! \class FastFourierTransformAlgorithm spl.h
    //! \brief Abstract prototype for all FFT implementations.
    //! \details This class provides a protected auxiliary method for implementing the CSVD.
    class SPL_API FastFourierTransformAlgorithm;
    //! \class RecursiveFFT spl.h
    //! \brief Implements the Cooley-Tukey FFT processing input data recursively.
    //! \details This implementation uses the C++11 and C++14 standards.
    class SPL_API RecursiveFFT;
    //! \class IterativeFFT spl.h
    //! \brief Implements Cooley-Tukey FFT processing input data iteratively.
    //! \details This implementation uses the C++11 and C++14 standards.
    class SPL_API IterativeFFT;

#ifdef USE_INTEL_TBB
    //! \class ParallelFFT spl.h
    //! \brief Implements the Cooley-Tukey FFT using Intel Threading Building Blocks for multithreading and multicore processor
    //! optimization.
    //! \note To use this implementation defining the macro \p \b USE_INTEL_TBB is required.
    class SPL_API ParallelFFT;
#endif

    enum class SPL_API FFT {
        RECURSIVE, 
        ITERATIVE, 
#ifdef USE_INTEL_TBB
        PARALLEL
#endif
    };

    //! \class FastFourierTransform spl.h
    //! \brief Unified interface for the Fast Fourier Transform. 
    //! \details This class cannot be inherited.
    class SPL_API FastFourierTransform final {
    public:
        //! \brief Default constructor.
        FastFourierTransform();
#if 0
        //! \brief Constructor.
        //! \param[in] ffta A pointer to a valid FFT implementation.
        //! \sa FastFourierTransformAlgorithm
        //! \sa RecursiveFFT
        //! \sa IterativeFFT
        //! \sa ParallelFFT
        FastFourierTransform(FastFourierTransformAlgorithm* ffta);
        //! \brief Destructor.
        ~FastFourierTransform();
        //! \brief Allows changing the algorithm used for calculating the FFT.
        //! \param[in] ffta The FFT implementation to be used.</param>
        //! \sa FastFourierTransformAlgorithm
        //! \sa RecursiveFFT
        //! \sa IterativeFFT
        //! \sa ParallelFFT
        void set_algorithm(FastFourierTransformAlgorithm* ffta);
        //! \brief Calculates the Fast Fourier Transform for a given set of data.
        //! \exception std::invalid_argument Thrown if the length of data is not a power of two.
        //! \param[in,out] data The data of which the FFT will be calculated.
        //! \param[in] forward Whether to calculate the forward (true) or backward (false) Fast Fourier Transform.
#endif
        FastFourierTransform(FFT fft);
        ~FastFourierTransform();
        void set_algorithm(FFT fft);
        void fft(std::vector<std::complex<double>>& data, bool forward = true) const;
    private:
        //! \brief Unique pointer to the FFT implementation used in the context.
        std::unique_ptr<FastFourierTransformAlgorithm> _ffta;
    };

    //==============================================================================================================//

    //! \class ComplexSingularValueDecompositionAlgorithm spl.h
    //! \brief Abstract prototype for all CSVD implementations.
    //! \details This class provides several protected auxiliary methods for implementing the CSVD.
    class SPL_API ComplexSingularValueDecompositionAlgorithm;
    //! \class FullCSVD spl.h
    //! \brief Implements the Full CSVD using Householder transformations and Givens rotations.
    //! \details This implementation uses the C++11 and C++14 standards.
    class SPL_API FullCSVD;


#ifdef USE_INTEL_TBB
    //! \class ParallelCSVD spl.h
    //! \brief Implements the Full CSVD using Intel Threading Building Blocks for multithreading and multicore processor
    //! optimization.
    //! \note To use this implementation defining the macro \p \b USE_INTEL_TBB is required.
    class SPL_API ParallelCSVD;
#endif

    enum class SPL_API CSVD {
        FULL,
#ifdef USE_INTEL_TBB
        PARALLEL
#endif
    };

    //! \class ComplexSingularValueDecomposition spl.h
    //! \brief Unified interface for the Complex Singular Value Decomposition.
    //! \details This class cannot be inherited.
    class SPL_API ComplexSingularValueDecomposition final {
    public:
        //! \brief Default constructor.
        ComplexSingularValueDecomposition();
#if 0
        //! \brief Constructor.
        //! \param[in] csvda A pointer to a valid CSVD implementation.
        //! \sa FullCSVD
        //! \sa ParallelCSVD
        ComplexSingularValueDecomposition(ComplexSingularValueDecompositionAlgorithm* csvda);
        //! \brief Destructor.
        ~ComplexSingularValueDecomposition();
        //! \brief Allows changing the algorithm used for calculating the CSVD.
        //! \param[in] csvda The CSVD implementation to be used.
        //! \sa ComplexSingularValueDecompositionAlgorithm
        //! \sa FullCSVD 
        //! \sa ParallelCSVD
        void set_algorithm(ComplexSingularValueDecompositionAlgorithm* csvda);
        //! \brief Allows setting the precision. The default precision is 1.0E-6.
        //! \param[in] epsilon The new precision.
#endif
        ComplexSingularValueDecomposition(CSVD csvd);
        ~ComplexSingularValueDecomposition();
        void set_algorithm(CSVD csvd);
        void set_precision(const double epsilon);
        //! \brief Allows to define a new iteration limit. 
        //! \details If this iteration limit is exceeded while calculating the CSVD an exception will be thrown.
        //! The default iteration limit is set to 10.000 (ten thousand) iterations.
        //! \sa ComplexSingularValueDecomposition#csvd
        void set_iteration_limit(unsigned long long iteration_limit);
        //! \brief Calculates the Singular Value Decomposition of an arbitrary complex matrix.
        //! \details All matrices and results are stored in single row array format.
        //! \warning If the given dimensions of the matrix are incorrect, the behaviour is undefined.
        //! \exception std::runtime_error In case the iteration limit is exceeded.
        //! \param[in] a The matrix for which the SVD will be calculated.
        //! \param[in] m The amount of rows of the input matrix \p a.
        //! \param[in] n The amount of columns of the input matrix \p a.
        //! \param[out] u The left eigenvectors as a \p m x \p m matrix.
        //! \param[out] sigma The eigenvalues as \p m x \p n matrix.
        //! \param[out] v The right eigenvectors as \p n x \p n matrix.
        void csvd(const std::vector<std::complex<double>>& a, int m, int n, std::vector<std::complex<double>>& u, std::vector<std::complex<double>>& sigma, std::vector<std::complex<double>>& v) const;
    private:
        //! \brief A unique pointer to a CSVD implementation.
        std::unique_ptr<ComplexSingularValueDecompositionAlgorithm> _csvda;
    };

    //==============================================================================================================//

    //! \class SingularValueDecompositionAlgorithm spl.h
    //! \brief Abstract prototype for all SVD implementations.
    //! \details This class provides several protected auxiliary methods for implementing the SVD.
    class SPL_API SingularValueDecompositionAlgorithm;
    //! \class FullSVD spl.h
    //! \brief Implements the Full SVD using Householder transformations and Givens rotations.
    //! \details This implementation uses the C++11 and C++14 standards.
    class SPL_API FullSVD;

#ifdef USE_INTEL_TBB
    //! \class ParallelSVD spl.h
    //! \brief Implements the Full SVD using Intel Threading Building Blocks for multithreading and multicore processor
    //! optimization.
    //! \note To use this implementation defining the macro \p \b USE_INTEL_TBB is required.
    class SPL_API ParallelSVD;
#endif

    //! \class SingularValueDecomposition spl.h
    //! \brief Unified interface for the Singular Value Decomposition.
    //! \details This class cannot be inherited.
    class SPL_API SingularValueDecomposition final {
    public:
        //! \brief Default constructor.
        SingularValueDecomposition();
        //! \brief Constructor.
        //! \param[in] svda A pointer to a valid SVD implementation.
        SingularValueDecomposition(SingularValueDecompositionAlgorithm* svda);
        //! \brief Destructor.
        ~SingularValueDecomposition();
        //! \brief Allows changing the algorithm used for calculating the SVD.
        //! \param[in] svda The SVD implementation to be used.
        //! \sa SingularValueDecompositionAlgorithm
        //! \sa FullSVD 
        //! \sa ParallelSVD
        void set_algorithm(SingularValueDecompositionAlgorithm* svda);
        //! \brief Allows setting the precision. The default precision is 1.0E-6.
        //! \param[in] epsilon The new precision.
        void set_precision(const double epsilon);
        //! \brief Allows to define a new iteration limit. 
        //! \details If this iteration limit is exceeded while calculating the CSVD an exception will be thrown.
        //! The default iteration limit is set to 10.000 (ten thousand) iterations.
        //! \sa ComplexSingularValueDecomposition#csvd
        void set_iteration_limit(const unsigned long long iteration_limit);
        //! \brief Calculates the Singular Value Decomposition of an arbitrary real matrix.
        //! \details All matrices and results are stored in single row array format.
        //! \warning If the given dimensions of the matrix are incorrect, the behaviour is undefined.
        //! \exception std::runtime_error In case the iteration limit is exceeded.
        //! \param[in] a The matrix for which the SVD will be calculated.
        //! \param[in] m The amount of rows of the input matrix \p a.
        //! \param[in] n The amount of columns of the input matrix \p a.
        //! \param[out] u The left eigenvectors as a \p m x \p m matrix.
        //! \param[out] sigma The eigenvalues as \p m x \p n matrix.
        //! \param[out] v The right eigenvectors as \p n x \p n matrix.
        //! \sa SingulatValueDecomposition#set_precision
        //! \sa SingularValueDecomposition#set_iteration_limit
        void svd(const std::vector<double>& a, int m, int n, std::vector<double>& u, std::vector<double>& sigma, std::vector<double>& v) const;
    private:
        //! \brief A unique pointer to an SVD implementation.
        std::unique_ptr<SingularValueDecompositionAlgorithm> _svda;
    };

    //==============================================================================================================//

    //! \class SystemOfLinearEquationsSolvingAlgorithm spl.h
    //! \brief Abstract prototype for all SLE solving algorithm implementations.
    //! \details This class provides several protected auxiliary methods for implementing algorithms that solve SLEs.
    class SPL_API SystemOfLinearEquationsSolvingAlgorithm;

    //! \class KaczmarzMethod spl.h
    //! \brief Implements the Kaczmarz method for solving systems of linear equations.
    //! \details This implementation uses the C++11 and C++14 standards.
    class SPL_API KaczmarzMethod;

    //! \class SystemOfLinearEquationsSolver spl.h
    //! \brief Unified interface for algorithms for solving systems of linear equations.
    //! \details This class cannot be inherited.
    class SPL_API SystemOfLinearEquationsSolver final {
    public:
        //! \brief Default constructor.
        SystemOfLinearEquationsSolver();
        //! \brief Constructor.
        //! \param[in] solesa A pointer to a valid SLE solving algorithm implementation.
        //! \sa SystemOfLinearEquationsSolvingAlgorithm
        //! \sa KaczmarzMethod 
        SystemOfLinearEquationsSolver(SystemOfLinearEquationsSolvingAlgorithm* solesa);
        //! \brief Destructor.
        ~SystemOfLinearEquationsSolver();
        //! \brief Allows changing the algorithm used for calculating the solution of a SLE.
        //! \param[in] solesa The SLE solving algorithm implementation to be used.
        //! \sa SystemOfLinearEquationsSolvingAlgorithm
        //! \sa KaczmarzMethod 
        void set_algorithm(SystemOfLinearEquationsSolvingAlgorithm* solesa);
        //! \brief Allows setting the precision. The default precision is 1.0E-6.
        //! \param[in] epsilon The new precision.
        void set_precision(const double epsilon);
        //! \brief Allows to define a new iteration limit. 
        //! \details If this iteration limit is exceeded while calculating the SLE solution an exception will be thrown.
        //! The default iteration limit is set to 10.000 (ten thousand) iterations.
        //! \sa SystemOfLinearEquationsSolvingAlgorithm#linear_solve
        void set_iteration_limit(const unsigned long long iteration_limit);
        //! \brief Calculates the solution vector of a system of linear equations (SLE).
        //! \details The coefficient matrix is stored in single row array format.
        //! \warning If the given dimensions of the matrix are incorrect, the behaviour is undefined.
        //! \exception std::runtime_error In case the iteration limit is exceeded.
        //! \param[in] a The matrix of coefficients of the SLE.
        //! \param[in] m The amount of rows of the input matrix \p a.
        //! \param[in] n The amount of columns of the input matrix \p a.
        //! \param[in] b The vector of constant terms of the SLE.
        //! \param[out] x The solution vector of the SLE.
        //! \sa SystemOfLinearEquationsSolver#set_precision
        //! \sa SystemOfLinearEquationsSolver#set_iteration_limit
        void sle_solve(std::vector<double> a, unsigned int m, unsigned int n, std::vector<double> b, std::vector<double>& x) const noexcept(false);
    private:
        //! \brief A unique pointer to an SLE solving algorithm implementation.
        std::unique_ptr<SystemOfLinearEquationsSolvingAlgorithm> _solesa;
    };

    //==============================================================================================================//

    /*class SPL_API ComplexPolynomialRootFindingAlgorithm;
    class SPL_API CPoly;

    class SPL_API ComplexPolynomialRootFinder final {
    public:
        void set_algorithm(ComplexPolynomialRootFindingAlgorithm* cprfa);
        void set_precision(const double epsilon);
        void find_roots(std::vector<std::complex<double>> c) const;
    private:
        std::unique_ptr<ComplexPolynomialRootFindingAlgorithm> _cprfa;
    };

    //==============================================================================================================//

    /*class SPL_API PolynomialRootFindingAlgorithm;
    class SPL_API RPoly;

    class SPL_API PolynomialRootFinder final {
    public:
        void set_algorithm(PolynomialRootFindingAlgorithm* prfa);
        void set_precision(const double epsilon);
        void find_roots(std::vector<double> c) const;
    private:
        std::unique_ptr<PolynomialRootFindingAlgorithm> _prfa;
    };*/

}

#endif