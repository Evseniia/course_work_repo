#pragma once
#include "pch.h"
#include "framework.h"
#include "FUNC.h"
#include "SIGNAL_GENERATION.h"
#include "OTHER_FUNC.h"
#include "afxdialogex.h"
#include <vector>
#define M_PI 3.1415926535

//////////////////////////////////////////////////////////////////////////////////////////////////////////
void addNoize_Complex(vector<complex<double>>& mass, double Noize)
{
	vector<double> shum_ampl;
	shum_ampl.resize(mass.size());
	for (int i = 0; i < shum_ampl.size(); i++)
	{
		shum_ampl[i] = 0;
	}
	double sum_signal = 0;
	double sum_shum = 0;
	for (int i = 0; i < mass.size(); i++)
	{
		sum_signal += mass[i].real() * mass[i].real() + mass[i].imag() * mass[i].imag();
	}
	for (int i = 0; i < mass.size(); i++)
	{
		double M, ksi;
		M = rand() % 9 + 12;
		ksi = 0;
		for (int k = 1; k <= M; k++)
		{
			ksi += (double)((rand() % 21 - 10) / 10.);
		}
		shum_ampl[i] = ksi / M;
	}
	vector<complex<double>> shum_c(shum_ampl.size());
	for (int i = 0; i < shum_c.size(); i++)
	{
		double r_phi = (rand() / RAND_MAX) * 2 * M_PI;
		shum_c[i] = shum_ampl[i] * cos(r_phi) + complex<double>(0, 1) * sin(r_phi);
	}
	for (int i = 0; i < mass.size(); i++)
	{
		sum_shum += shum_c[i].real() * shum_c[i].real() + \
			shum_c[i].imag() * shum_c[i].imag();
	}
	sum_signal = sqrt(sum_signal);
	sum_shum = sqrt(sum_shum);

	double alfa = sum_signal / (sum_shum * (pow(10, Noize / 20.)));
	for (int i = 0; i < mass.size(); i++)
	{
		mass[i] += alfa * shum_c[i] + complex<double>(0, 1)*alfa * shum_c[i];
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
void fur(vector <complex <double>>& data, int is)
{
	int i, j, istep, n;
	n = data.size();
	int m, mmax;
	double r, r1, theta, w_r, w_i, temp_r, temp_i;
	double pi = 3.1415926f;

	r = pi * is;
	j = 0;
	for (i = 0; i < n; i++)
	{
		if (i < j)
		{
			temp_r = data[j].real();
			temp_i = data[j].imag();
			data[j] = data[i];
			data[i] = temp_r + complex <double>(0, 1) * temp_i;

		}
		m = n >> 1;
		while (j >= m) { j -= m; m = (m + 1) / 2; }
		j += m;
	}
	mmax = 1;
	while (mmax < n)
	{
		istep = mmax << 1;
		r1 = r / (double)mmax;
		for (m = 0; m < mmax; m++)
		{
			theta = r1 * m;
			w_r = (double)cos((double)theta);
			w_i = (double)sin((double)theta);
			for (i = m; i < n; i += istep)
			{
				j = i + mmax;
				temp_r = w_r * data[j].real() - w_i * data[j].imag();
				temp_i = w_r * data[j].imag() + w_i * data[j].real();
				data[j] = (data[i].real() - temp_r) + complex <double>(0, 1) * (data[i].imag() - temp_i);
				data[i] += (temp_r)+complex <double>(0, 1) * (temp_i);
			}
		}
		mmax = istep;
	}
	if (is > 0)
		for (i = 0; i < n; i++)
		{
			data[i] /= (double)n;
		}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
int step2(int sizein)
{
	int i = 0;
	double S = sizein;
	for (;;)
	{
		if (S > 1)
		{
			i++;
			S /= 2;
		}
		else break;
	}
	return pow(2, i);
}
int step2(int sizein, int& i)
{
	i = 0;
	double S = sizein;
	for (;;)
	{
		if (S > 1)
		{
			i++;
			S /= 2;
		}
		else break;
	}
	return pow(2, i);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////
void kv_interpolation(vector<double>& mass, double& max_i_w, double& max_i_interpolation, int size)
{
	vector <double> interpolation;
	interpolation.clear();
	vector<double> interp;
	interp.clear();

	double max = 0, max1 = 0;
	double x0 = 0, x1 = 0, x2 = 0, y0 = 0, y1 = 0, y2 = 0, a = 0, b = 0, c = 0;

	for (int i = 0; i < mass.size(); i++)
	{
		if (mass[i] > max)
		{
			max = mass[i];
			max_i_w = i;
		}
	}
	max_i_w = size - max_i_w;
	interp.push_back(mass[max_i_w - 1]);
	interp.push_back(mass[max_i_w]);
	interp.push_back(mass[max_i_w + 1]);

	x0 = max_i_w - 1;
	x1 = max_i_w;
	x2 = max_i_w + 1;
	y0 = interp[0];
	y1 = interp[1];
	y2 = interp[2];

	a = ((y2 - y0) * (x1 - x0) - (y1 - y0) * (x2 - x0)) / ((x2 * x2 - x0 * x0) * (x1 - x0) - (x1 * x1 - x0 * x0) * (x2 - x0));
	b = (y1 - y0 - a * (x1 * x1 - x0 * x0)) / (x1 - x0);
	c = y0 - (a * x0 * x0 + b * x0);

	double buf = 0;
	for (double i = x0; i < x2; i += 0.001)
	{
		buf = 0;
		buf = a * i * i + b * i + c;
		interpolation.push_back(buf);
	}
	for (double i = x0; i < x2; i += 0.001)
	{
		if (interpolation[i] > max1)
		{
			max1 = interpolation[i];
			max_i_interpolation = i;
		}

	}
	max_i_interpolation = (max_i_w - 1) + max_i_interpolation / interpolation.size();
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////
int svd_hestenes(int m_m, int n_n, vector<double>& a,
	vector<double>& u, vector<double>& v, vector<double>& sigma)
{
	double thr = 1.E-4f, nul = 1.E-16f;
	int n, m, i, j, l, k, lort, iter, in, ll, kk;
	double alfa, betta, hamma, eta, t, cos0, sin0, buf, s;
	n = n_n;
	m = m_m;
	for (i = 0; i < n; i++)
	{
		in = i * n;
		for (j = 0; j < n; j++)
			if (i == j) v[in + j] = 1.;
			else v[in + j] = 0.;
	}
	for (i = 0; i < m; i++)
	{
		in = i * n;
		for (j = 0; j < n; j++)
		{
			u[in + j] = a[in + j];
		}
	}

	iter = 0;
	while (1)
	{
		lort = 0;
		iter++;
		for (l = 0; l < n - 1; l++)
			for (k = l + 1; k < n; k++)
			{
				alfa = 0.; betta = 0.; hamma = 0.;
				for (i = 0; i < m; i++)
				{
					in = i * n;
					ll = in + l;
					kk = in + k;
					alfa += u[ll] * u[ll];
					betta += u[kk] * u[kk];
					hamma += u[ll] * u[kk];
				}

				if (sqrt(alfa * betta) < nul)	continue;
				if (fabs(hamma) / sqrt(alfa * betta) < thr) continue;

				lort = 1;
				eta = (betta - alfa) / (2.f * hamma);
				t = (double)((eta / fabs(eta)) / (fabs(eta) + sqrt(1. + eta * eta)));
				cos0 = (double)(1. / sqrt(1. + t * t));
				sin0 = t * cos0;

				for (i = 0; i < m; i++)
				{
					in = i * n;
					buf = u[in + l] * cos0 - u[in + k] * sin0;
					u[in + k] = u[in + l] * sin0 + u[in + k] * cos0;
					u[in + l] = buf;

					if (i >= n) continue;
					buf = v[in + l] * cos0 - v[in + k] * sin0;
					v[in + k] = v[in + l] * sin0 + v[in + k] * cos0;
					v[in + l] = buf;
				}
			}

		if (!lort) break;
	}

	for (i = 0; i < n; i++)
	{
		s = 0.;
		for (j = 0; j < m; j++)	s += u[j * n + i] * u[j * n + i];
		s = (double)sqrt(s);
		sigma[i] = s;
		if (s < nul)	continue;
		for (j = 0; j < m; j++)	u[j * n + i] /= s;
	}
	//======= Sortirovka ==============
	for (i = 0; i < n - 1; i++)
		for (j = i; j < n; j++)
			if (sigma[i] < sigma[j])
			{
				s = sigma[i]; sigma[i] = sigma[j]; sigma[j] = s;
				for (k = 0; k < m; k++)
				{
					s = u[i + k * n]; u[i + k * n] = u[j + k * n]; u[j + k * n] = s;
				}
				for (k = 0; k < n; k++)
				{
					s = v[i + k * n]; v[i + k * n] = v[j + k * n]; v[j + k * n] = s;
				}
			}

	return iter;
}
///////////////////////////////////////////////////////////////////////////////////////////////////
int ___CSVD(vector<complex<double>> A, int M, int N, int NU, int NV, vector<double>& S,
	vector<complex<double>>& U, vector<complex<double>>& V)
{
	int i, j, k, k1, kk, kc, kc1, l, l1, ll;
	int nmax;
	int IP = 0, NP, N1;
	double Q_re, Q_im, R_re, R_im, Z, W, tmp, tmp1, EPS, CS, SN, F, H;
	double X, Y, G;
	//double* B, * C, * T;
	vector <double> B, C, T;
	double ETA = 1.2E-7, TOL = 2.4E-16;
	int index, index1;

	nmax = N;
	if (M > nmax) nmax = M;
	//B = new double[nmax];
	//C = new double[nmax];
	//T = new double[nmax];
	B.resize(nmax);
	C.resize(nmax);
	T.resize(nmax);

	for (i = 0; i < nmax; i++)
	{
		B[i] = 0;
		C[i] = 0;
		T[i] = 0;
	}

	NP = N + IP;
	N1 = N + 1;
	//	ponivenie porqdka
	C[0] = 0;
	k = 1;
m10:
	k1 = k + 1;

	kc = k - 1;
	kc1 = k1 - 1;
	//	iskl`~enie |lementow a(I,K),I=K+1,...,M
	Z = 0;
	for (i = kc; i < M; i++) { index = i * N + kc; Z += A[index].real() * A[index].real() + A[index].imag() * A[index].imag(); }
	B[kc] = 0;
	if (Z <= TOL) goto m70;
	Z = (double)sqrt(Z);
	B[kc] = Z;
	index = kc * N + kc;
	W = (double)sqrt(A[index].real() * A[index].real() + A[index].imag() * A[index].imag());
	Q_re = 1; Q_im = 0;
	if (fabs(W) >= TOL * TOL) { Q_re = A[index].real() / W; Q_im = A[index].imag() / W; }
	A[index] = std::complex<double>((double)(Q_re * (Z + W)), (double)(Q_im * (Z + W)));

	if (k == NP) goto m70;

	for (j = kc1; j < NP; j++)
	{
		Q_re = 0; Q_im = 0;
		for (i = kc; i < M; i++)
		{
			index = i * N + j;
			index1 = i * N + kc;
			Q_re += A[index].real() * A[index1].real() + A[index].imag() * A[index1].imag();
			Q_im += A[index].imag() * A[index1].real() - A[index].real() * A[index1].imag();
		}
		Q_re /= (Z * (Z + W));
		Q_im /= (Z * (Z + W));

		for (i = kc; i < M; i++)
		{
			index = i * N + j;
			index1 = i * N + kc;
			tmp = Q_re * A[index1].real() - Q_im * A[index1].imag();
			tmp1 = Q_im * A[index1].real() + Q_re * A[index1].imag();
			A[index] -= std::complex<double>((double)tmp, (double)tmp1);
		}
	}
	//	preobrazowanie fazy
	index = kc * N + kc;
	tmp = (double)sqrt(A[index].real() * A[index].real() + A[index].imag() * A[index].imag());
	Q_re = -A[index].real() / tmp;
	Q_im = A[index].imag() / tmp;

	for (j = kc1; j < NP; j++)
	{
		index = kc * N + j;
		tmp = Q_re * A[index].real() - Q_im * A[index].imag();
		tmp1 = Q_im * A[index].real() + Q_re * A[index].imag();
		A[index] = std::complex<double>((double)tmp, (double)tmp1);
	}
	//	iskl`~enie |lementow a(K,J),J=K+2,...,N
m70:
	if (k == N) goto m140;
	Z = 0;
	for (j = kc1; j < N; j++) { index = kc * N + j; Z += A[index].real() * A[index].real() + A[index].imag() * A[index].imag(); }

	C[kc1] = 0;
	if (Z <= TOL) goto m130;
	Z = (double)sqrt(Z);
	C[kc1] = Z;
	index = kc * N + kc1;
	W = (double)sqrt(A[index].real() * A[index].real() + A[index].imag() * A[index].imag());
	Q_re = 1; Q_im = 0;
	if (fabs(W) > TOL * TOL) { Q_re = A[kc * N + kc1].real() / W; Q_im = A[kc * N + kc1].imag() / W; }
	A[index] -= std::complex<double>((double)(Q_re * (Z + W)), (double)(Q_im * (Z + W)));

	for (i = kc1; i < M; i++)
	{
		Q_re = 0; Q_im = 0;
		for (j = kc1; j < N; j++)
		{
			index = i * N + j;
			index1 = kc * N + j;
			Q_re += A[index].real() * A[index1].real() + A[index].imag() * A[index1].imag();
			Q_im += A[index].imag() * A[index1].real() - A[index].real() * A[index1].imag();
		}
		Q_re /= (Z * (Z + W));
		Q_im /= (Z * (Z + W));

		for (j = kc1; j < N; j++)
		{
			index = i * N + j;
			index1 = kc * N + j;
			tmp = Q_re * A[index1].real() - Q_im * A[index1].imag();
			tmp1 = Q_im * A[index1].real() + Q_re * A[index1].imag();
			A[index] -= std::complex<double>((double)tmp, (double)tmp1);
		}
	}

	//	preobrazowanie fazy
	index = kc * N + kc1;
	tmp = (double)sqrt(A[index].real() * A[index].real() + A[index].imag() * A[index].imag());
	Q_re = -A[index].real() / tmp;
	Q_im = A[index].imag() / tmp;

	for (i = kc1; i < M; i++)
	{
		index = i * N + kc1;
		tmp = Q_re * A[index].real() - Q_im * A[index].imag();
		tmp1 = Q_im * A[index].real() + Q_re * A[index].imag();
		A[index] = std::complex<double>((double)tmp, (double)tmp1);
	}
m130:
	k = k1;
	goto m10;
	//	dopusk dlq prenebrevimo malyh |lementow
m140:
	EPS = 0;
	for (k = 0; k < N; k++)
	{
		S[k] = (double)B[k];
		T[k] = C[k];
		if ((S[k] + T[k]) > EPS) EPS = S[k] + T[k];
	}
	EPS = (double)(EPS * ETA);
	//	inicializaciq wy~islenij U i V
	if (NU == 0) goto m180;

	for (j = 0; j < NU; j++)
	{
		for (i = 0; i < M; i++)
		{
			U[i * M + j] = 0;
		}
		U[j * M + j] = std::complex<double>(1, 0);
	}
m180:
	if (NV == 0) goto m210;
	for (j = 0; j < NV; j++)
	{
		for (i = 0; i < N; i++)
		{
			V[i * N + j] = 0;
		}
		V[j * N + j] = std::complex<double>(1, 0);
	}

	//	QR-diagonalizaciq
m210:
	for (kk = 1; kk <= N; kk++)
	{
		k = N1 - kk;
		//	prowerka na ras}eplenie
	m220:
		for (ll = 1; ll <= k; ll++)
		{
			l = k + 1 - ll;
			if ((double)fabs(T[l - 1]) <= EPS) goto m290;
			if ((double)fabs(S[l - 2]) <= EPS) goto m240; // l-1 ???
		}
		//	sokra}enie B(L)
	m240:
		CS = 0;
		SN = 1;
		l1 = l - 1;
		for (i = l; i <= k; i++)
		{
			F = SN * T[i - 1];
			T[i - 1] = CS * T[i - 1];
			if ((double)fabs(F) <= EPS) goto m290;
			H = S[i - 1];
			W = (double)sqrt(F * F + H * H);
			S[i - 1] = (double)W;
			CS = H / W;
			SN = -F / W;
			if (NU == 0) goto m260;
			for (j = 0; j < N; j++)
			{
				index = j * M + l1 - 1;
				index1 = j * M + i - 1;
				X = U[index].real();
				Y = U[index1].real();
				U[index] = std::complex<double>((double)(X * CS + Y * SN), 0);
				U[index1] = std::complex<double>((double)(Y * CS - X * SN), 0);
			}
		m260:
			if (NP == N) continue;
			for (j = N1; j <= NP; j++)
			{
				index = (l1 - 1) * N + j - 1;
				index1 = (i - 1) * N + j - 1;
				Q_re = A[index].real();
				Q_im = A[index].imag();
				R_re = A[index1].real();
				R_im = A[index1].imag();
				A[index] = std::complex<double>((double)(Q_re * CS + R_re * SN), (double)(Q_im * CS + R_im * SN));
				A[index1] = std::complex<double>((double)(R_re * CS - Q_re * SN), (double)(R_im * CS - Q_im * SN));
			}
		}

		//	prowerka shodimosti
	m290:
		W = S[k - 1];
		if (l == k) goto m360;
		//	sdwig na~ala koordinat
		X = S[l - 1];
		Y = S[k - 2];
		G = T[k - 2];
		H = T[k - 1];
		F = ((Y - W) * (Y + W) + (G - H) * (G + H)) / (2 * H * Y);
		G = (double)sqrt(F * F + 1);
		if (F < 0) G = -G;
		F = ((X - W) * (X + W) + (Y / (F + G) - H) * H) / X;
		//	QR {ag
		CS = 1;
		SN = 1;
		l1 = l + 1;
		for (i = l1; i <= k; i++)
		{
			G = T[i - 1];
			Y = S[i - 1];
			H = SN * G;
			G = CS * G;
			W = (double)sqrt(H * H + F * F);
			T[i - 2] = W;
			CS = F / W;
			SN = H / W;
			F = X * CS + G * SN;
			G = G * CS - X * SN;
			H = Y * SN;
			Y = Y * CS;
			if (NV == 0) goto m310;
			for (j = 0; j < N; j++)
			{
				index = j * N + i - 1;
				X = V[index - 1].real();
				W = V[index].real();
				V[index - 1] = std::complex<double>((double)(X * CS + W * SN), 0);
				V[index] = std::complex<double>((double)(W * CS - X * SN), 0);
			}

		m310:
			W = (double)sqrt(H * H + F * F);
			S[i - 2] = (double)W;
			CS = F / W;
			SN = H / W;
			F = CS * G + SN * Y;
			X = CS * Y - SN * G;
			if (NU == 0) goto m330;
			for (j = 0; j < N; j++)
			{
				index = j * M + i - 1;
				Y = U[index - 1].real();
				W = U[index].real();
				U[index - 1] = std::complex<double>((double)(Y * CS + W * SN), 0);
				U[index] = std::complex<double>((double)(W * CS - Y * SN), 0);
			}

		m330:
			if (NP == N) continue;
			for (j = N1; j <= NP; j++)
			{
				index = (i - 2) * N + j - 1;
				index1 = (i - 1) * N + j - 1;
				Q_re = A[index].real();
				Q_im = A[index].imag();
				R_re = A[index1].real();
				R_im = A[index1].imag();
				A[index] = std::complex<double>((double)(Q_re * CS + R_re * SN), (double)(Q_im * CS + R_im * SN));
				A[index1] = std::complex<double>((double)(R_re * CS - Q_re * SN), (double)(R_im * CS - Q_im * SN));
			}
		}
		T[l - 1] = 0;
		T[k - 1] = F;
		S[k - 1] = (double)X;
		goto m220;
		//	shodimostx
	m360:
		if (W >= 0) continue;
		S[k - 1] = (double)-W;
		if (NV == 0) continue;
		for (j = 0; j < N; j++)
		{
			index = j * N + k - 1;
			V[index] = -V[index];
		}
	}

	//	uporqdo~enie singulqrnyh ~isel
	for (k = 0; k < N; k++)
	{
		G = -1;
		j = k;
		for (i = k; i < N; i++)
		{
			if (S[i] <= G) continue;
			G = S[i];
			j = i;
		}
		if (j == k) continue;
		S[j] = S[k];
		S[k] = (double)G;
		if (NV == 0) goto m410;
		for (i = 0; i < N; i++)
		{
			index = i * N + j;
			index1 = i * N + k;
			std::swap(V[index], V[index1]);
		}
	m410:
		if (NU == 0) goto m430;
		for (i = 0; i < N; i++)
		{
			index = i * M + j;
			index1 = i * M + k;
			std::swap(U[index], U[index1]);
		}
	m430:
		if (NP == N) continue;
		for (i = N1 - 1; i < NP; i++)
		{
			index = j * N + i;
			index1 = k * N + i;
			std::swap(A[index], A[index1]);
		}
	}
	//	obratnoe preobrazowanie
	if (NU == 0) goto m510;
	for (kk = 1; kk <= N; kk++)
	{
		k = N1 - kk;
		kc = k - 1;
		if (B[kc] == 0) continue;
		index = kc * N + kc;
		tmp = (double)sqrt(A[index].real() * A[index].real() + A[index].imag() * A[index].imag());
		Q_re = -A[index].real() / tmp;
		Q_im = -A[index].imag() / tmp;
		for (j = 0; j < NU; j++)
		{
			index = kc * M + j;
			tmp = Q_re * U[index].real() - Q_im * U[index].imag();
			tmp1 = Q_im * U[index].real() + Q_re * U[index].imag();
			U[index] = std::complex<double>((double)tmp, (double)tmp1);
		}
		for (j = 0; j < NU; j++)
		{
			Q_re = 0; Q_im = 0;
			for (i = kc; i < M; i++)
			{
				index = i * M + j;
				index1 = i * N + kc;
				Q_re += (U[index].real() * A[index1].real() + U[index].imag() * A[index1].imag());
				Q_im += (U[index].imag() * A[index1].real() - U[index].real() * A[index1].imag());
			}
			index = kc * N + kc;
			tmp = B[kc] * (double)sqrt(A[index].real() * A[index].real() + A[index].imag() * A[index].imag());
			Q_re /= tmp;
			Q_im /= tmp;
			for (i = kc; i < M; i++)
			{
				index = i * M + j;
				index1 = i * N + kc;
				tmp = Q_re * A[index1].real() - Q_im * A[index1].imag();
				tmp1 = Q_im * A[index1].real() + Q_re * A[index1].imag();
				U[index] -= std::complex<double>((double)tmp, (double)tmp1);
			}
		}
	}
m510:
	if (NV == 0) goto m570;
	if (N < 1) goto m570;
	for (kk = 2; kk <= N; kk++)
	{
		k = N1 - kk;
		kc = k - 1;
		k1 = k + 1;
		kc1 = k1 - 1;
		if (C[kc1] == 0) continue;
		index = kc * N + kc1;
		tmp = (double)sqrt(A[index].real() * A[index].real() + A[index].imag() * A[index].imag());
		Q_re = -A[index].real() / tmp;
		Q_im = A[index].imag() / tmp;
		for (j = 0; j < NV; j++)
		{
			index = kc1 * N + j;
			tmp = Q_re * V[index].real() - Q_im * V[index].imag();
			tmp1 = Q_im * V[index].real() + Q_re * V[index].imag();
			V[index] = std::complex<double>((double)tmp, (double)tmp1);
		}
		for (j = 0; j < NV; j++)
		{
			Q_re = 0; Q_im = 0;
			for (i = kc1; i < N; i++)
			{
				index = i * N + j;
				index1 = kc * N + i;
				Q_re += (A[index1].real() * V[index].real() - A[index1].imag() * V[index].imag());
				Q_im += (A[index1].imag() * V[index].real() + A[index1].real() * V[index].imag());
			}
			index = kc * N + kc1;
			tmp = C[kc1] * (double)sqrt(A[index].real() * A[index].real() + A[index].imag() * A[index].imag());
			Q_re /= tmp;
			Q_im /= tmp;

			for (i = kc1; i < N; i++)
			{
				index = i * N + j;
				index1 = kc * N + i;
				tmp = Q_re * A[index1].real() + Q_im * A[index1].imag();
				tmp1 = Q_im * A[index1].real() - Q_re * A[index1].imag();
				V[index] -= std::complex<double>((double)tmp, (double)tmp1);
			}

		}
	}
m570: IP = 0;

	//delete[] B;
	//delete[] C;
	//delete[] T;
	B.clear();
	C.clear();
	T.clear();

	return IP;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////
void trans_matr(const vector<vector<complex<double>>>& v1, 
	const vector<vector<complex<double>>>& v2, vector<vector<complex<double>>>& v3)
{
	vector<vector<complex<double>>> buffer;
	buffer.clear();
	buffer.resize(v1.size());
	for (int i = 0; i < buffer.size(); i++)
	{
		buffer[i].resize(v2[0].size());
		for (int j = 0; j < buffer[i].size(); j++)
		{
			for (int r = 0; r < v2.size(); r++)
				buffer[i][j] += v1[i][r] * (v2[r][j]);
		}
	}
	v3 = buffer;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////
void transpose_conj(vector<vector<complex<double>>>& v1)
{
	vector<vector<complex<double>>> buffer; buffer.resize(v1[0].size());
	for (int i = 0; i < buffer.size(); i++)
	{
		buffer[i].resize(v1.size());
		for (int j = 0; j < buffer[i].size(); j++)
		{
			buffer[i][j] = conj(v1[j][i]);
		}
	}
	v1 = buffer;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////
void gauss_jordan(vector<vector<complex<double>>>& Corr, int razmer_matr_corr)
{
	//����� �������� �������
	vector<vector<complex<double>>> AObr;
	AObr.resize(razmer_matr_corr);
	for (int i = 0; i < razmer_matr_corr; i++)
	{
		AObr[i].resize(razmer_matr_corr);
		for (int j = 0; j < razmer_matr_corr; j++)
		{
			AObr[i][j] = Corr[i][j];
		}
	}

	//���������� ������� L ��� ��������� �������:
	vector<vector<complex<double>>> L;
	L.resize(razmer_matr_corr);
	for (int i = 0; i < razmer_matr_corr; i++)
	{
		L[i].resize(razmer_matr_corr);
		for (int j = 0; j < razmer_matr_corr; j++)
		{
			if (i != j) L[i][j] = 0;
			else L[i][j] = 1;
		}
	}

	//��������������� ��� ������ ������ �������� ������� � ��������� ������� ���������� 
	//��������� ��������: 
	//1)������� ������� ������ ������� �� ������� �������
	//2)���������� ��������� ������� ��� ������� ���������
	//3)���������� ��������� ������� ��� ������� ���������
	for (int i = 0; i < razmer_matr_corr; i++)
	{
		//�������� �� ����� �� ������� ������� 0. ���� �����, �� ���� ������ �
		//��������� i-� ��������� � ������ �� ������� � ������� i-� �������:
		complex <double> p = Corr[i][i];
		if (abs(p) == 0)
		{
			int j = i + 1;
			while ((abs(Corr[j][i]) == 0) && (j < razmer_matr_corr))
				j++;
			if (j < razmer_matr_corr)
			{
				//���� ������ �������, ������ �������� ������� � ��������� �����:
				for (int k = 0; k < razmer_matr_corr; k++)
				{
					complex <double> Temp = Corr[i][k];
					Corr[i][k] = Corr[j][k];
					Corr[j][k] = Temp;
				}
				p = Corr[i][i];
			}
			//���� ��������� ������� �� ������, ������ �������� ������� - �����������:
			else
			{
				string error = "�������� ������� �����������!";
				break;
			}
		}
		//������� ������� ������ �������� � ��������� ������� �� ������� �������:
		for (int j = 0; j < razmer_matr_corr; j++)
		{
			Corr[i][j] = Corr[i][j] / p;
			L[i][j] = L[i][j] / p;
		} //for j

		//���������� ��������� �������� � ��������� ������� ��� ������� ���������:
		for (int k = 0; k < i; k++)
		{
			p = Corr[k][i];
			for (int j = 0; j < razmer_matr_corr; j++)
			{
				L[k][j] = L[k][j] - p * L[i][j];
				Corr[k][j] = Corr[k][j] - p * Corr[i][j];
			} //for j
		} //for k

		//���������� ��������� �������� � ��������� ������� ��� ������� ���������:
		for (int k = i + 1; k < razmer_matr_corr; k++)
		{
			p = Corr[k][i];
			for (int j = 0; j < razmer_matr_corr; j++)
			{
				L[k][j] = L[k][j] - p * L[i][j];
				Corr[k][j] = Corr[k][j] - p * Corr[i][j];
			} //for j
		} //for k
	}

	//����� ��������� ���������� ������� � �����:
	for (int i = 0; i < razmer_matr_corr; i++)
	{
		for (int j = 0; j < razmer_matr_corr; j++)
		{
			AObr[i][j] = L[i][j]; //���������� �������� ������� 
		}
	}

	Corr = AObr;
	AObr.clear();
}
/////////////////////////////////////////////////////////////////////////////////////////////////////