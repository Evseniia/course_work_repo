﻿#pragma once
#include "pch.h"
#include "framework.h"
#include "FUNC.h"
#include "SIGNAL_GENERATION.h"
#include "OTHER_FUNC.h"
#include "afxdialogex.h"
#include <vector>
#define M_PI 3.1415926535

////////////////////////////////////////////////////////////////////////////////////////////////////////////
double Func_neopr_Complex_Fur(vector<double>& mass_t, vector<double>& mass_w,
	vector<complex<double>>& Signal1, vector<complex<double>>& Signal2,
	int k, double chast_diskr)
{
	int max_i, max_j;
	double Max;
	int razmer_sig1 = step2(Signal1.size());

	vector <complex<double>> Rr;
	vector <complex<double>> Rr_new;
	vector <double> neopr_real; // переход к 1D вектору
	int neopr_real_step; //шаг внутри одномерного вектора (длина одной строки)

	mass_t.clear();
	mass_w.clear();

	Rr.resize(razmer_sig1);
	Rr_new.resize(razmer_sig1 / k);
	complex<double> buf;

	neopr_real_step = Rr_new.size();
	int aaa = Signal2.size() - Signal1.size();
	neopr_real.resize(aaa * neopr_real_step); // модуль

	for (int i = 0; i < Signal2.size() - Signal1.size(); i++)
	{
		complex<double> sub_const = 0;
		for (int j = 0; j < Signal1.size(); j++)
		{
			Rr[j] = (Signal1[j] * conj(Signal2[i + j]));
		}
		if (k == 1)Rr_new = Rr;
		else
			for (int j = 0; j < razmer_sig1 / k; j++)
			{
				buf = 0;
				for (int i = 0; i < k; i++)
				{
					buf += Rr[j * k + i];
				}
				Rr_new[j] = buf;
				sub_const += Rr_new[j];
			}
		sub_const /= Rr_new.size();
		for (int j = 0; j < Rr_new.size(); j++) Rr_new[j] -= sub_const;

		fur(Rr_new, -1);
		for (int j = 0; j < Rr_new.size(); j++)
		{
			neopr_real[i * neopr_real_step + j] = abs(Rr_new[j]);
		}
	}

	Max = 0;
	max_i = 0;
	max_j = 0;
	std::vector<double>::iterator result;
	result = max_element(neopr_real.begin(), neopr_real.end());
	max_i = distance(neopr_real.begin(), result); // номер максимального элемента в 1D векторе
	max_j = max_i % neopr_real_step;
	max_i = (max_i - max_j) / neopr_real_step;
	if (max_j > neopr_real_step / 2) max_j = neopr_real_step - max_j;

	//for (int i = 0; i < Signal1.size(); i++)
	//{
	//	mass_t.push_back(neopr_real[i * neopr_real_step + max_j]);
	//}
	for (int i = 0; i < neopr_real_step; i++)
	{
		mass_w.push_back(neopr_real[max_i * neopr_real_step + i]);
	}
	double my_w_debug = (chast_diskr / mass_w.size()) * ((double)max_j / k);
	return my_w_debug;
//	double Max;
//	int razmer_sig1 = step2(Signal1.size());
//
//	vector <complex<double>> Rr;
//	vector <complex<double>> Rr_new;
//	vector <vector<double>> neopr_real;
//
//
//
//	mass_t.clear();
//	mass_w.clear();
//
//	Rr.resize(razmer_sig1);
//	Rr_new.resize(razmer_sig1 / k);
//	complex<double> buf;
//
//	neopr_real.resize(Signal1.size()); // ������
//	for (int i = 0; i < neopr_real.size(); i++)
//	{
//		neopr_real[i].resize(Rr_new.size());
//	}
//
//	for (int i = 0; i < Signal1.size(); i++)
//	{
//		for (int j = 0; j < Rr.size(); j++)Rr[j] = 0;
//		for (int j = 0; j < Signal1.size(); j++)
//		{
//			if ((i + j) == Signal2.size())
//			{
//				break;
//			}
//			Rr[j] = (Signal1[j] * conj(Signal2[j + i]));
//			buf += Rr[j];
//		}
//
//		buf /= Rr.size();
//		for (int j = 0; j < Signal1.size(); j++)
//		{
//			Rr[j] -= buf;
//		}
//
//		if (k == 1)Rr_new = Rr;
//		else
//			for (int j = 0; j < razmer_sig1 / k; j++)
//			{
//				buf = 0;
//				for (int i = 0; i < k; i++)
//				{
//					buf += Rr[j * k + i];
//				}
//				Rr_new[j] = buf;
//			}
//
//		fur(Rr_new, -1);
//
//		for (int j = 0; j < Rr_new.size(); j++)
//		{
//			neopr_real[i][j] = abs(Rr_new[j]);
//		}
//	}
//	Max = 0;
//	int max_i = 0;
//	int max_j = 0;
//
//	for (int i = 0; i < neopr_real.size(); i++)
//	{
//		for (int j = 0; j < neopr_real[i].size(); j++)
//		{
//			if (neopr_real[i][j] > Max)
//			{
//				Max = neopr_real[i][j];
//				max_i = i;
//				if (j < neopr_real[i].size() / 2) max_j = j;
//				else max_j = neopr_real[i].size() - j;
//			}
//		}
//	}
//	for (int i = 0; i < neopr_real.size(); i++)
//	{
//		mass_t.push_back(neopr_real[i][max_j]);
//	}
//	mass_w = neopr_real[max_i];
//	double my_w_debug = (chast_diskr / mass_w.size()) * ((double)max_j / k);
//return my_w_debug;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////
double Func_neopr_Complex_Music(vector<double>& mass_w,
	vector<complex<double>>& Signal1, vector<complex<double>>& Signal2,
	double chast_diskr, double max_fur, double porog)
{

	double Max;
	int razmer_sig1 = Signal1.size();

	vector <complex<double>> Rr;
	vector <double> neopr_real; // переход к 1D вектору
	int neopr_real_step; //шаг внутри одномерного вектора (длина одной строки)
	vector<complex<double>> Music;

	mass_w.clear();

	Rr.resize(razmer_sig1);
	complex<double> buf;

	neopr_real_step = DIAPOZON_DOTS;
	neopr_real.resize(Signal1.size() * neopr_real_step); // модуль
	for (int i = 0; i < Signal2.size() - Signal1.size(); i++)
	{
		complex<double> sub_const = 0;
		for (int j = 0; j < Rr.size(); j++)
		{
			Rr[j] = (Signal1[j] * conj(Signal2[i + j]));
			sub_const += Rr[j];
		}

		sub_const /= Rr.size();
		for (int j = 0; j < Rr.size(); j++) Rr[j] -= sub_const;

		method_Music(Rr, Music, chast_diskr, max_fur, DIAPOZON, porog);
		for (int j = 0; j < Music.size(); j++)
		{
			neopr_real[i * neopr_real_step + j] = abs(Music[j]);
		}
	}

	Max = 0;
	int max_i = 0;
	int max_j = 0;
	std::vector<double>::iterator result;
	result = max_element(neopr_real.begin(), neopr_real.end());
	max_i = distance(neopr_real.begin(), result); // номер максимального элемента в 1D векторе
	max_j = max_i % neopr_real_step;
	max_i = (max_i - max_j) / neopr_real_step;
	if (max_j > neopr_real_step / 2) max_j = neopr_real_step - max_j;

	for (int i = 0; i < neopr_real_step; i++)
	{
		mass_w.push_back(neopr_real[max_i * neopr_real_step + i]);
	}
	return (max_fur - DIAPOZON) + (2 * DIAPOZON / Music.size()) * max_j;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////
double Func_neopr_Complex_Capon(vector<double>& mass_w,
	vector<complex<double>>& Signal1, vector<complex<double>>& Signal2,
	double chast_diskr, double max_fur)
{
	double Max;
	int razmer_sig1 = Signal1.size();

	vector <complex<double>> Rr;
	vector <double> neopr_real; // переход к 1D вектору
	int neopr_real_step; //шаг внутри одномерного вектора (длина одной строки)
	vector<complex<double>> Capon;

	mass_w.clear();

	Rr.resize(razmer_sig1);
	complex<double> buf;

	neopr_real_step = DIAPOZON_DOTS;
	neopr_real.resize(Signal1.size() * neopr_real_step); // модуль
	for (int i = 0; i < Signal2.size() - Signal1.size(); i++)
	{
		complex<double> sub_const = 0;
		for (int j = 0; j < Rr.size(); j++)
		{
			Rr[j] = (Signal1[j] * conj(Signal2[i + j]));
			sub_const += Rr[j];
		}

		sub_const /= Rr.size();
		for (int j = 0; j < Rr.size(); j++) Rr[j] -= sub_const;
		method_Capon(Rr, Capon, chast_diskr, max_fur, DIAPOZON);
		for (int j = 0; j < Capon.size(); j++)
		{
			neopr_real[i * neopr_real_step + j] = abs(Capon[j]);
		}
	}

	Max = 0;
	int max_i = 0;
	int max_j = 0;
	std::vector<double>::iterator result;
	result = max_element(neopr_real.begin(), neopr_real.end());
	max_i = distance(neopr_real.begin(), result); // номер максимального элемента в 1D векторе
	max_j = max_i % neopr_real_step;
	max_i = (max_i - max_j) / neopr_real_step;
	if (max_j > neopr_real_step / 2) max_j = neopr_real_step - max_j;

	for (int i = 0; i < neopr_real_step; i++)
	{
		mass_w.push_back(neopr_real[max_i * neopr_real_step + i]);
	}
	return (max_fur - DIAPOZON) + (2 * DIAPOZON / Capon.size()) * max_j;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
void Func_neopr_modified(vector<double>& mass_w, vector<complex<double>>& Signal1,
	vector<complex<double>>& Signal2, int k, double chast_diskr, int choise_method, double porog, double& max_j_fur, double& max_j_method)
{
	vector<double> mass_t; mass_t.clear();
	vector<double> mass_ww; mass_ww.clear();

	max_j_fur = Func_neopr_Complex_Fur(mass_t, mass_ww, Signal1, Signal2, k, chast_diskr);
	mass_ww.clear(); mass_t.clear();

	//if (choise_method == 1)
	//{
	//	max_j_method = Func_neopr_Complex_Music(mass_w, Signal1, Signal2, chast_diskr, max_j_fur, porog);
	//}
	//if (choise_method == 2)
	//{
	//	max_j_method = Func_neopr_Complex_Capon(mass_w, Signal1, Signal2, chast_diskr, max_j_fur);
	//}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////
void method_Music(const vector<complex<double>>& Signal,
	vector<complex<double>>& Music, double chast_diskr, double max_j, double shag_w, double porog)
{
	int razmer_sig1 = Signal.size() / MUSIC;

	vector<complex<double>> Rr;


	Music.clear();

	Rr.resize(razmer_sig1);
	double buf;
	for (int i = 0; i < Rr.size(); i++)
	{
		for (int j = 0; j < /*Signal.size() - i*/Rr.size(); j++)
		{
			complex<double>  buffer_size = razmer_sig1;
			Rr[i] += (Signal[j] * conj(Signal[i + j])) / buffer_size;
		}
	}
	int M = Rr.size();
	vector<complex<double>> Matr_Corr;
	Matr_Corr.resize(M * M);
	for (int i = 0; i < M; i++)
	{
		for (int j = 0; j < M; j++)
		{
			int l = abs(i - j);
			Matr_Corr[i * M + j] = Rr[l];;
		}
	}

	vector<complex<double>> U, V;
	vector<double> sigma;
	V.resize(M * M); U.resize(M * M); sigma.resize(M);
	___CSVD(Matr_Corr, M, M, M, M, sigma, U, V);
	//double porog = 0.01 * sigma[0];
	int P = 0;
	double max = sigma[0];
	double min = sigma[sigma.size() - 1];
	double P_value = min + (max - min) * porog;
	for (int j = 1; j < sigma.size(); j++)
	{
		if (sigma[j] < P_value)
		{
			P = j;
			break;
		}
	}

	vector <double> w; w.clear(); // вектор частот
	for (double j = max_j - shag_w; j < max_j + shag_w; j += (2 * DIAPOZON) / (DIAPOZON_DOTS - 1))
	{
		w.push_back(j);
	}
	double summ;
	for (int i = 0; i < w.size(); i++)
	{
		complex<double> summ = 0;////////
		vector<complex<double>> e_f;
		for (int t = 0; t < M; t++) // комплексные экспоненты (сопряженные)
		{
			double T = 1. / chast_diskr;
			complex<double> ex = cos(2 * M_PI * w[i] * t * T) \
				- complex <double>(0, 1) * sin(2 * M_PI * w[i] * t * T);
			e_f.push_back(ex);
		}
		//summ = 0;
		//for (int j = P + 1; j < M; j++)
		//{
		//	for (int k = 0; k < e_f.size(); k++) // скалярное произведение
		//	{
		//		complex <double> little_sum = e_f[k] * V[j + k * M];
		//		summ += pow(abs(little_sum), 2);
		//	}
		//}
		//Music.push_back(1. / summ);
		for (int j = P + 1; j < M; j++)
		{
			complex<double> buffer;
			for (int k = 0; k < e_f.size(); k++) // скалярное произведение
			{
				buffer += e_f[k] * V[j + k * M];
			}
			summ += buffer;
		}
		double summ_d = pow(abs(summ), 2);
		Music.push_back(1. / summ_d);
	}
	//Signal.clear();
	Rr.clear();
	Matr_Corr.clear();
	U.clear(); V.clear(); sigma.clear();
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////
void method_Capon(vector<complex<double>>& Signal,
	vector<complex<double >>& Vec, double chast_diskr, double max_j, double shag_w)
{
	int razmer_sig1 = Signal.size() / CAPON;

	vector <complex<double>> Rr;
	vector<vector<complex<double>>> Corr;

	Vec.clear();

	Rr.resize(razmer_sig1);
	double buf;

	Corr.resize(Rr.size());
	for (int i = 0; i < Corr.size(); i++)
	{
		Corr[i].resize(Rr.size());
	}

	for (int i = 0; i < Rr.size(); i++)
	{
		for (int j = 0; j < Rr.size(); j++)
		{
			if ((i + j) == razmer_sig1)
			{
				break;
			}
			Rr[i] += Signal[j] * conj(Signal[i + j]);

		}

	}
	for (int i = 0; i < Rr.size(); i++)
	{
		for (int j = 0; j < Rr.size(); j++)
		{
			int l = abs(i - j);
			Corr[i][j] = Rr[l];
		}
	}

	int razmer_matr_corr = Corr.size();

	gauss_jordan(Corr, razmer_matr_corr);

	vector <double> w; w.clear(); // вектор частот
	for (double j = max_j - shag_w; j < max_j + shag_w; j += (2 * DIAPOZON) / (DIAPOZON_DOTS - 1))
	{
		w.push_back(j);
	}
	for (int i = 0; i < w.size(); i++)
	{
		vector<vector<complex<double>>> e_f; e_f.resize(1);
		for (int t = 0; t < razmer_matr_corr; t++) // комплексные экспоненты (сопряженные)
		{
			double T = 1. / chast_diskr;
			complex<double> ex = cos(2 * M_PI * w[i] * t * T) \
				- complex <double>(0, 1) * sin(2 * M_PI * w[i] * t * T);
			e_f[0].push_back(ex);
		}
		vector<vector<complex<double>>> buffer;
		trans_matr(e_f, Corr, buffer);
		transpose_conj(e_f);
		trans_matr(buffer, e_f, buffer);

		Vec.push_back(1. / buffer[0][0]);
	}
}
///////////////////////////////////////////////////
//////////////2020/////////////////////////////////
void convolution_omp(vecc_d& h, vecc_d& x, vecc_d& result)
{
	result.clear();
	result.resize(x.size() - h.size(),0);
#pragma omp parallel for
	for (int i = 0; i < result.size(); i++)
	{
		for (int j = 0; j < h.size(); j++)
		{   
			result[i] += x[i + j] * h[j];
		}
	}
}
int circular_convolution_via_dft(vecc_d &h, vecc_d &x, vecc_d& result)
{
	result.clear();
	int conv_size = x.size() - h.size();
	int L = step2(x.size());
	x.resize(L);
	h.resize(L);
	fur(x, -1);
	fur(h, -1);
#pragma omp parallel for
	for (int i = 0; i < L; i++) x[i] *= h[h.size()-1-i];
	fur(x, 1);
	//x.resize(conv_size);
	result=x;
	return 0;
}
void sectional_convolution(vecc_d &h, vecc_d &x, vecc_d& result)
{
	result.clear();
	int M = h.size();
	int s;
	int L = 8* step2(M);// L >= N+M etc 2M
	h.resize(L);
	fur(h, -1);
	int conv_count = (x.size()-M) / M;
	result.resize(conv_count * M);
	vector<vecc_d> results; results.resize(conv_count);
	x.resize(x.size() + h.size());
#pragma omp parallel for
	for (int i = 0; i < conv_count; i++)
	{
		results[i].resize(L);
#pragma omp parallel for
		for (int j = 0; j < L; j++) results[i][j] = x[j + i * M];
		fur(results[i], -1);
#pragma omp parallel for
		for (int j = 0; j < L; j++) results[i][j] *= h[L-1-j];
		fur(results[i], 1);
#pragma omp parallel for
		for (int j = 0; j < M; j++) result[j + i * M] = results[i][j];
	}
}
double Filter_Bank_Viewpoint(const vecc_d& Signal_1, const vecc_d& Signal_2,
	double freq_min, double freq_max,
	double freq_step, int sampling, vector<double>& pic)
{
	vecc_d Signal_2_flip = Signal_2;
	reverse(Signal_2_flip.begin(), Signal_2_flip.end());
	vector<double> matr_svertok;
	int freq_dots = (freq_max - freq_min) / freq_step;
	int conv_size = Signal_2_flip.size() - Signal_1.size();
	matr_svertok.resize(conv_size * freq_dots);
	for (int i = 0; i < freq_dots; i++)
	{
		vecc_d Signal_1_local = Signal_1;
		double local_freq = freq_min + freq_step * i;
		Dopler_shift(Signal_1_local, sampling, local_freq);
		vecc_d Signal_2_flip_local = Signal_2_flip;
		vecc_d conv;
		//convolution_omp(Signal_1_local, Signal_2_flip_local, conv);
		sectional_convolution(Signal_1_local, Signal_2_flip_local, conv);
		conv_size = conv.size();
		for (int j = 0; j < conv_size; j++)
			matr_svertok[j + i * conv_size] = (abs(conv[j]));
	}
	int max_index = std::max_element(matr_svertok.begin(), matr_svertok.end()) - matr_svertok.begin();
	int ostatok = max_index % conv_size; // для среза по частотам 
	max_index = max_index / conv_size; // индекс вектора с макс элементом
	double my_w_debug = freq_min + freq_step * max_index;
	///////PIC
	pic.resize(freq_dots);
	for (int i = 0; i < pic.size(); i++)
	{
		pic[i] = matr_svertok[ostatok + i * conv_size];
	}
	return my_w_debug;
}
double Filter_Bank_Viewpoint_v2(const vecc_d& Signal_1, const vecc_d& Signal_2,
	double &freq_min, double &freq_max,
	double freq_step, int sampling, vector<double>& pic, int iter) 
{
	if (iter < 1) return 0;
	double delta_w_finded;
	int i = 0;
	for (;;)
	{
		delta_w_finded = Filter_Bank_Viewpoint(Signal_1, Signal_2, freq_min, freq_max, freq_step, sampling, pic);
		if (delta_w_finded<freq_min || delta_w_finded>freq_max) return 0;
		double freq_range = (freq_max - freq_min) / 2.;
		freq_step /= 2.;	
		i++;
		if (i == iter)break;
		freq_min = delta_w_finded - freq_range / 2.;
		freq_max = delta_w_finded + freq_range / 2.;
	}
	return delta_w_finded;
}
